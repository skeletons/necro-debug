# Necro OCaml Debugger

## Preview

![Preview](debug_js.png)

## Installation

### Via `opam`

You can install `necrolib` via `opam`. It is not in the official opam
repository yet, so it is necessary to first add our repository.

```bash
opam switch create necro 4.14.1
eval $(opam env)
opam repository add necro https://gitlab.inria.fr/skeletons/opam-repository.git#necro
opam install necrolib
```

### From the sources

#### Dependencies

* `OCaml` (4.12 or greater)
* `dune`
* `necrolib`
* `js_of_ocaml`
* `js_of_ocaml-ppx`

## How-to use it

`necrodebug myfile.sk -o backend.ml` will create an OCaml file called `backend.ml`.
This file defines an abstract machine to debug your skeletal semantics. You
can use this machine with one of the available front-end, available in
[files](./files)

See example folder
