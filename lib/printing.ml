open Format
open Necro
open TypedAST
open Util




let ocaml_keywords = SSet.of_list
	["and"; "as"; "asr"; "assert"; "begin"; "class"; "constraint"; "do"; "done";
	"downto"; "else"; "end"; "exception"; "external"; "false"; "for"; "fun";
	"function"; "functor"; "if"; "in"; "include"; "inherit"; "initializer";
	"land"; "lazy"; "let"; "lor"; "lsl"; "lsr"; "lxor"; "match"; "method"; "mod";
	"module"; "open"; "mutable"; "new"; "nonrec"; "object"; "of"; "open"; "or";
	"private"; "rec"; "sig"; "struct"; "then"; "to"; "true"; "try"; "type"; "val";
	"virtual"; "when"; "while"; "with"]

let necro_debug_keywords = SSet.empty

let reserved = SSet.union ocaml_keywords necro_debug_keywords




(********** Helper functions **********)

let if_empty res arg =
	if arg = "" then res else arg

let max_list min l =
	List.fold_left (fun a b -> max a b) min l

let list_init min max f =
	if min > max then [] else
	List.init (max - min + 1) (fun i -> f (i+min))






let aux arg ta =
	begin match ta with
	| [] -> ""
	| _ :: [] -> arg ^ " "
	| _ :: q -> List.fold_left (fun s _ -> s ^ ", " ^ arg) ("("^arg) q ^ ") "
	end

let print_type_list print_type ta =
	let rec aux ta =
		begin match ta with
		| [] -> ""
		| ({tydesc=Arrow _;_} as a) :: [] ->
				"(" ^ print_type a ^ ")"
		| a :: [] -> print_type a
		| ({tydesc=Arrow _;_} as a) :: q ->
				"(" ^ print_type a ^ ") * " ^ aux q
		| a :: q -> print_type a ^ " * " ^ aux q
		end
	in
	begin match ta with
	| [] -> "unit"
	| a :: [] -> print_type a
	| _ -> "(" ^ aux ta ^ ")"
	end

let print_type_args print_type ta =
	let rec aux ta =
		begin match ta with
		| [] -> ""
		| a :: [] -> print_type a
		| a :: q -> print_type a ^ ", " ^ aux q
		end
	in
	begin match ta with
	| [] -> ""
	| _ :: [] -> aux ta ^ " "
	| _ -> "(" ^ aux ta ^ ") "
	end

let rec print_type ?(prefix="") ty =
	begin match ty.tydesc with
	| Variable v -> "'" ^ v
	| UserType ((n, _), ta) ->
			sprintf "%s%s%s"
			(print_type_args (print_type ~prefix) ta) prefix n
	| Product tl -> print_type_list (print_type ~prefix) tl
	| Arrow (({tydesc=Arrow _;_} as i), o) ->
			sprintf "(%s) -> %s" (print_type ~prefix i) (print_type ~prefix o)
	| Arrow (i, o) ->
			sprintf "%s -> %s" (print_type ~prefix i) (print_type ~prefix o)
	end

let spec_types sem =
	let rec print_constr cl =
		begin match cl with
		| [] -> ""
		| cs :: q when cs.cs_input_type.tydesc = Product [] ->
				sprintf "\n\t| %s%s"
				cs.cs_name (print_constr q)
		| cs :: q -> sprintf "\n\t| %s of %s%s"
				cs.cs_name (print_type cs.cs_input_type) (print_constr q)
		end
	in
	let rec print_field fl =
		begin match fl with
		| [] -> ""
		| fs :: q -> sprintf "\n\t%s: %s;%s"
				fs.fs_name (print_type fs.fs_field_type) (print_field q)
		end
	in
	let print_one_type fst n def =
		begin match def with
		| TDVariant (ta, cl) -> ksprintf (fun x -> (false, x)) "\n\t%s %s%s =%s"
		(if fst then "type" else "and")
		(print_type_args (fun x -> "'" ^ x) ta) n (print_constr cl)
		| TDAlias (ta, ty) -> ksprintf (fun x -> (false, x)) "\n\t%s %s%s = %s"
		(if fst then "type" else "and")
		(print_type_args (fun x -> "'" ^ x) ta) n (print_type ty)
		| TDRecord (ta, fl) -> ksprintf (fun x -> (false, x)) "\n\t%s %s%s =
			{%s}"
		(if fst then "type" else "and")
		(print_type_args (fun x -> "'" ^ x) ta) n (print_field fl)
		| _ -> (fst, "")
		end
	in
	let (s, b) =
		List.fold_left (fun (str,fst) (n, (_, def)) ->
			let (fst, print) = print_one_type fst n def in
			(str ^ print, fst)) ("",true) (SMap.bindings sem.ss_types)
	in
	if b then s else s ^ "\n"

let unspec_types sem =
	let print_one_type n def =
		begin match def with
		| TDUnspec 0 ->
				sprintf "\n\ttype %s"  n
		| TDUnspec ta ->
				sprintf "\n\ttype %s%s_unpacked" (aux "_" (List.init ta (Fun.const ""))) n
		| _ -> ""
		end
	in
	List.fold_left (fun str (n, (_, def)) ->
		str ^ print_one_type n def) "" (SMap.bindings sem.ss_types)

let breakpoint_options sem =
	let print_one_term (n, _) =
		sprintf "\n\t\t\t^ \"<option value=\\\"%s\\\">%s</option>\"" n n
	in
	SMap.bindings sem.ss_terms
	|> List.map print_one_term
	|> String.concat ""

let print_ _sem n =
	let protect x =
		let ss_kw = SSet.empty
		(* Skeleton.ss_reserved_names sem TODO*) in
		fresh (fun x -> not (SSet.mem x reserved || SSet.mem x ss_kw)) x
	in
	protect ("print_" ^ n)

let unspec_terms sem =
	let rec print_list print l =
		begin match l with
		| [] -> "unit"
		| a :: [] -> print a
		| a :: q -> print a ^ " * " ^ print_list print q
		end
	in
	let rec print_type_args print ta =
		let rec aux ta =
			begin match ta with
			| [] -> ""
			| a :: [] -> print a
			| a :: q -> print a ^ ", " ^ aux q
			end
		in match ta with | [] -> "" | [_] -> aux ta ^ " " | _ -> "(" ^ aux ta ^ ") "
	and print_left_type ty =
		begin match ty.tydesc with
		| Arrow _ -> failwith "unspecified terms with functions in argument are not allowed in debugger"
		| Variable a -> "'" ^ a
		| Product tl -> sprintf "(%s)" (print_list print_left_type tl)
		| UserType ((n, _), ta) ->
				print_type_args print_left_type ta ^ n
		end
	and print_type ty =
		begin match ty.tydesc with
		| Arrow (i, o) ->
				print_left_type i ^ " -> " ^ print_type o
		| UserType ((n, _), ta) ->
				print_type_args print_left_type ta ^ n
		| Product tl -> sprintf "(%s)" (print_list print_type tl)
		| Variable a -> "'" ^ a
		end
	in
	let print_one_term n t ta ty =
		begin match t with
		| None ->
				begin try
					let type_args = if ta = [] then "" else
						List.map (sprintf "'%s") ta
						|> String.concat " "
						|> Format.sprintf "%s. "
					in
					sprintf "\n\tval %s: %s%s" n type_args (print_type ty)
				with _ ->
					failwith ("Error with " ^ n ^ " : unspecified terms with \
					functions in argument are not allowed in debugger")
				end
		| _ -> ""
		end
	in
	let s =
		List.fold_left (fun str (n, (_, (ta, ty, t))) ->
			str ^ print_one_term n t ta ty) "" (SMap.bindings sem.ss_terms)
	in if s = "" then "" else s ^ "\n"

let rec print_list sep print l cpt =
	begin match l with
	| [] -> ""
	| a :: [] -> print cpt a
	| a :: q -> print cpt a ^ sep ^ print_list sep print q (cpt+1)
	end

(* add a packer to polymorphic non-specified types *)
let packing sem fst =
	let pack_one_type fst n def =
		begin match def with
		| TDUnspec i when i > 0 ->
			let args =
				if i = 1 then "'a" else
				sprintf "(%s)"
				(list_init 1 i (fun i -> sprintf "'a%d" i) |> String.concat ", ")
			in
			let t = sprintf "%s %s" args n in
			let constructor = sprintf "Pack_%s" n in
			(* type ('a1, …, 'ai) t = | TPack of ('a1, …, 'ai) t_packed *)
			(false, sprintf "\t%s %s = %s of %s_unpacked\n" (if fst then "type" else "and") t constructor t)
		| _ -> (fst, "")
		end
	in
	let (_, pack) =
		List.fold_left (fun (fst, str) (n, (_, def)) ->
			let (fst, s) = pack_one_type fst n def in
			(fst, str ^ s)) (fst, "") (SMap.bindings sem.ss_types)
	in
	if pack = "" then "" else "\n" ^ pack

let print_module_type onlyunspec sem =
	let print_one_type n def =
		let (args, t, unspec) =
			begin match def with
			| TDUnspec 0 ->
					("", n, true)
			| TDUnspec 1 ->
					("('a -> String.t) ->", sprintf "'a %s" n, true)
			| TDUnspec i ->
					let args = (* ('a1 -> string) -> … -> ('ai -> string) -> *)
						list_init 1 i (fun i -> sprintf "('a%d -> String.t) -> " i)
						|> String.concat ""
					in
					let t = (* ('a1, …, 'ai) n *)
						let type_args =
							list_init 1 i (fun i -> sprintf "'a%d" i)
							|> String.concat ", "
						in
						sprintf "(%s) %s" type_args n
					in (args, t, true)
			| TDAlias (ta, _) | TDVariant (ta, _) | TDRecord (ta, _) ->
					let args = (* ('ta0 -> string) -> … -> ('tan -> string) -> *)
						List.map (fun a -> sprintf "('%s -> String.t) -> " a) ta
						|> String.concat ""
					in
					let t = (* ('a0, …, 'ai) n *)
						let type_args =
							List.map ((^) "'") ta
							|> String.concat ", "
						in
						begin match List.length ta with
						| 0 -> sprintf "%s" n
						| 1 -> sprintf "%s %s" type_args n
						| _ -> sprintf "(%s) %s" type_args n
						end
					in (args, t, false)
			end
		in
		(* val print_n:
				('a0 -> string) -> … -> ('ai -> string) -> ('a0, …, 'ai) n -> string *)
		if onlyunspec && not unspec then "" else
		sprintf "\tval %s: %s%s -> String.t\n"
		(print_ sem n) args t
	in
	let types =
		List.fold_left (fun str (n, (_, def)) ->
			str ^ print_one_type n def) "" (SMap.bindings sem.ss_types)
	in
	if types = "" then "" else "\n" ^ types

let default_print sem =
	let print_one_type n def =
		begin match def with
		| TDUnspec i ->
				sprintf "\tlet %s _ %s= \"(%s)\"\n"
				(print_ sem n) (list_init 1 i (Fun.const "_ ") |> String.concat "") n
		| _ -> ""
		end
	in
	let types =
		List.fold_left (fun str (n, (_, def)) ->
			str ^ print_one_type n def) "" (SMap.bindings sem.ss_types)
	in
	if types = "" then "" else "\n" ^ types

let spec_printers sem =
	let rec printer ty v =
		let noprint ty =
			begin match ty.tydesc with
			| Product [] | Arrow _ -> true
			| _ -> false
			end
		in
		begin match ty.tydesc with
		| Variable a ->
			begin match v with
			| Some v -> sprintf "(varprint_%s %s)" a v
			| None -> sprintf "varprint_%s" a
			end
		| Product [] ->
			begin match v with
			| Some _ -> "\"()\""
			| None -> "(fun _ -> \"()\")"
			end
		| Product [_] -> assert false (* impossible because of skel invariant *)
		| Product ta -> (* ta is [ta0; …; tan] *)
			let args = (* (v0, …, vn) where unit is () *)
				List.mapi (fun i tai ->
					if noprint tai then "_" else sprintf "v%d" i) ta
				|> String.concat ", " in
			let printers = (* (printer ta0 v0) … (printer tan vn) *)
				List.mapi (fun i tai -> printer tai (Some (sprintf "v%d" i))) ta
				|> String.concat " "
			in
			let percentss =
				List.map (Fun.const "%s") ta
				|> String.concat ", "
			in
			begin match v with
			| Some v -> sprintf "(let (%s) = %s in Format.sprintf \"(%s)\" %s)"
					args v percentss printers
			| None -> sprintf "(fun (%s) -> Format.sprintf \"(%s)\" %s)"
					args percentss printers
			end
		| UserType ((s, _), [])->
			begin match v with
			| Some v ->
				sprintf "(%s %s)" (print_ sem s) v
			| None ->
				sprintf "%s" (print_ sem s)
			end
		| UserType ((s, _), ta)->
			let print_args =
				List.map (fun x -> printer x None) ta
				|> String.concat " "
			in
			begin match v with
			| Some v ->
				sprintf "(%s %s %s)" (print_ sem s) print_args v
			| None ->
				sprintf "(%s %s)" (print_ sem s) print_args
			end
		| Arrow _ ->
				begin match v with
				| Some _ -> "\"(fun)\""
				| None -> "(fun _ -> \"(fun)\")"
				end
		end
	in
	let printv ty =
		begin match ty.tydesc with
		| Arrow _ | Product [] -> false
		| Product (_ :: _) | UserType _ | Variable _ -> true
		end
	in
	let print_one_type n def =
		begin match def with
		| TDVariant (ta, cl) ->
				(* print_t: … = fun varprint_ta1 … varprint_tan ->
 					begin function
 					| …
 					end *)
				let args = List.map (sprintf "varprint_%s ") ta |> String.concat "" in
				let constr =
						let v c =
							if printv c.cs_input_type then "v" else "_" in
						List.map (fun c ->
							if c.cs_input_type.tydesc = Product [] then
								sprintf "| %s -> \"%s \"" c.cs_name c.cs_name
							else
								sprintf "| %s %s -> \"%s \" ^ %s" c.cs_name (v c) c.cs_name (printer
							c.cs_input_type (Some "v"))) cl
						|> String.concat "\n\t"
				in
				begin match ta with
				| [] ->
					sprintf "%s %s=\n\
                  \tbegin function\n\
                  \t%s\n\
                  \tend"
						(print_ sem n) args constr
				| _ ->
					let sig_ =
						sprintf "type %s. %s%s%s -> String.t"
						(String.concat " " ta)
						(List.map (sprintf "(%s -> String.t) -> ") ta |> String.concat "")
						(ta |> String.concat ", "
						|> sprintf (if List.length ta = 1 then "%s " else "(%s) "))
						n
					in
					sprintf "%s: %s = fun %s->\n\
                  \tbegin function\n\
                  \t%s\n\
                  \tend"
						(print_ sem n) sig_ args constr
				end
		| TDRecord (ta, fl) ->
				(* print_t varprint_ta1 … varprint_tan =
 					fun {…} -> …
 					*)
				let args = List.map (sprintf "varprint_%s ") ta |> String.concat "" in
				let record = List.map (fun f -> f.fs_name) fl |> String.concat ";" in
				let recprint =
					List.map (fun f -> sprintf "%s=%%s" f.fs_name) fl
					|> String.concat ", "
				in
				let printf_args =
					List.map (fun f -> printer f.fs_field_type (Some f.fs_name)) fl
					|> String.concat "\n\t\t"
				in
				begin match ta with
				| [] ->
				sprintf "%s %s=\n\
                \tfun {%s} ->\n\
                \t\tFormat.sprintf \"(%s)\"\n\
                \t\t%s"
					(print_ sem n) args record recprint printf_args
				| _ ->
					let sig_ =
						sprintf "type %s. %s%s%s -> String.t"
						(String.concat " " ta)
						(List.map (sprintf "(%s -> String.t) -> ") ta |> String.concat "")
						(ta |> String.concat ", "
						|> sprintf (if List.length ta = 1 then "%s " else "(%s) "))
						n
					in
					sprintf "%s: %s = %s->\n\
                  \tfun {%s} ->\n\
                  \t\tFormat.sprintf \"(%s)\"\n\
                  \t\t%s"
						(print_ sem n) sig_ args record recprint printf_args
				end
		| TDAlias ([], ty) ->
				sprintf "%s v =\n\t\t%s"
					(print_ sem n) (printer ty (Some "v"))
		| TDAlias (ta, ty) ->
				let args = List.map (sprintf "varprint_%s ") ta |> String.concat "" in
				let sig_ =
					sprintf "type %s. %s%s%s -> String.t"
					(String.concat " " ta)
					(List.map (sprintf "(%s -> String.t) -> ") ta |> String.concat "")
					(ta |> String.concat ", "
					|> sprintf (if List.length ta = 1 then "%s " else "(%s) "))
					n
				in
				sprintf "%s: %s = fun %s v ->\n\t\t%s"
					(print_ sem n) sig_ args (printer ty (Some "v"))
		| _ -> ""
		end
	in
	let types =
		List.map (fun (n, (_, def)) ->
			print_one_type n def) (SMap.bindings sem.ss_types)
		|> List.filter ((<>)"") |> String.concat "\n\tand "
	in
	if types = "" then "" else "\n\tlet rec " ^ types ^ "\n"


let type_witness sem =
	let one_type_witness (s, (_, td)) =
		let ta, alias =
			begin match td with
			| TDUnspec 0 -> [], false
			| TDUnspec i -> list_init 1 i (sprintf "'a%d"), false
			| TDVariant (ta, _) -> List.map (sprintf "'%s") ta, false
			| TDRecord (ta, _) -> List.map (sprintf "'%s") ta, false
			| TDAlias (ta, _) -> List.map (sprintf "'%s") ta, true
			end
		in
		let args     = List.map (sprintf "%s witness") ta |> String.concat " * "
		and ta_print =
			begin match ta with
			| [] -> ""
			| [s] -> s ^ " "
			| _ -> sprintf "(%s) " @@ String.concat ", " ta
			end
		in
		let constr = String.capitalize_ascii s in
		if alias then [] else
		begin match args with
		| "" -> [sprintf "| %s: %s%s witness" constr ta_print s]
		| _  -> [sprintf "| %s: %s -> %s%s witness" constr args ta_print s]
		end
	in
	SMap.bindings sem.ss_types
	|> List.concat_map one_type_witness
	|> String.concat "\n\t\t"

let constr cl: string =
	let print_one_constr c =
		let output_type: typ =
			{tydesc=UserType (
				(c.cs_variant_type, None),
				List.map (fun x ->
					{tydesc=Variable x;tyloc=ghost_loc}) c.cs_type_args)
			;tyloc=ghost_loc}
		in
		let args =
			begin match c.cs_type_args with
			| [] -> ""
			| ta ->
					List.map (sprintf "'%s witness") ta
					|> String.concat " * "
					|> sprintf "%s -> "
			end
		in
		sprintf "| C%s: %s(%s, %s) constructor"
		c.cs_name
		args
		(print_type c.cs_input_type)
		(print_type output_type)
	in
	List.map print_one_constr cl
		|> String.concat "\n\t\t"
		|> if_empty "|"

let field fl: string =
	let print_one_field f =
		let record_type =
			{ tydesc=
			UserType (
				(f.fs_record_type, None),
				List.map (fun x -> {tydesc=Variable x;tyloc=ghost_loc}) f.fs_type_args)
			; tyloc=ghost_loc}
		in
		let args =
			begin match f.fs_type_args with
			| [] -> ""
			| ta ->
					List.map (sprintf "'%s witness") ta
					|> String.concat " * "
					|> sprintf "%s -> "
			end
		in
		sprintf "| F%s: %s(%s, %s) field"
		f.fs_name
		args
		(print_type record_type)
		(print_type f.fs_field_type)
	in
	List.map print_one_field fl
		|> String.concat "\n\t\t"
		|> if_empty "|"

let constr_type cl: string =
	let print_one_constr c =
		let args =
			begin match c.cs_type_args with
			| [] -> ""
			| ta -> String.concat ", " ta |> sprintf " (%s)"
			end
		in
		sprintf "| C%s%s -> %s%s" c.cs_name args
		(String.capitalize_ascii c.cs_variant_type) args
	in
	List.map print_one_constr cl
		|> String.concat "\n\t\t\t"
		|> if_empty "| _ -> ."

let constr_compute cl: string =
	let print_one_constr c =
		let args =
			begin match c.cs_type_args with
			| [] -> ""
			| _ -> " _"
			end
		in
		if c.cs_input_type.tydesc = Product [] then
			sprintf "| C%s%s -> let () = a in %s"
			c.cs_name args c.cs_name
		else
			sprintf "| C%s%s -> %s a"
			c.cs_name args c.cs_name
	in
	List.map print_one_constr cl
		|> String.concat "\n\t\t\t"
		|> if_empty "| _ -> ."

let field_compute fl: string =
	let print_one_field f =
		let args =
			begin match f.fs_type_args with
			| [] -> ""
			| _ -> " _"
			end
		in
		sprintf "| F%s%s -> a.%s"
		f.fs_name args f.fs_name
	in
	List.map print_one_field fl
		|> String.concat "\n\t\t\t"
		|> if_empty "| _ -> ."

let remove_constr cl: string =
	let print_one_constr c =
		let args =
			begin match c.cs_type_args with
			| [] -> ""
			| _ -> " _"
			end
		in
		if c.cs_input_type.tydesc = Product [] then
			sprintf "|C%s%s, %s -> Some ()" c.cs_name args c.cs_name
		else
			sprintf "| C%s%s, %s a -> Some a"
			c.cs_name args c.cs_name
	in
	List.map print_one_constr cl
		|> String.concat "\n\t\t\t"

let spec sem =
	let print_one_term (n, ta, ty) =
		let args =
			if ta = [] then "" else
			List.map (sprintf "'%s witness") ta
			|> String.concat " * "
			|> sprintf "%s -> "
		in
		sprintf "| S_%s: %s(%s) spec" n args (print_type ty)
	in
	SMap.bindings sem.ss_terms |>
	List.filter_map (fun (n, (_, (ta, ty, o))) ->
		Option.map (Fun.const (n, ta, ty)) o) |>
	List.map print_one_term
	|> String.concat "\n\t\t\t"
	|> if_empty "|"

let unspec sem =
	let print_one_term (n, ta, ty) =
		let args =
			if ta = [] then "" else
			List.map (sprintf "'%s witness") ta
			|> String.concat " * "
			|> sprintf "%s -> "
		in
		sprintf "| U_%s: %s(%s) unspec" n args (print_type ty)
	in
	SMap.bindings sem.ss_terms |>
	List.filter_map (fun (n, (_, (ta, ty, o))) ->
		Option.fold ~some:(Fun.const None) ~none:(Some (n, ta, ty)) o) |>
	List.map print_one_term
	|> String.concat "\n\t\t\t"
	|> if_empty "|"

let rec unalias sem (ty: typ):
			typ =
	begin match ty.tydesc with
	| UserType ((n, _), ta_got) ->
			let (_, td) =
				try SMap.find n sem.ss_types
				with Not_found -> Errors.type_error ty.tyloc
					(Unbound {typ="type";name=n})
			in
			begin match td with
			| TDUnspec _ | TDVariant _ | TDRecord _ -> ty
			| TDAlias (ta, ty) ->
				if List.compare_lengths ta ta_got <> 0 then
					Errors.type_error ty.tyloc (WrongNumberArgs
					{name=n;
					typ="type";
					given=List.length ta_got;
					expected=List.length ta}) ;
				unalias sem (Types.subst_mult ta ta_got ty)
			end
	| _ -> ty
	end

let rec unalias_rec sem (ty: typ):
			typ =
	let ret tydesc = {ty with tydesc} in
	begin match ty.tydesc with
	| Variable x -> ret @@ Variable x
	| UserType ((n, _), ta_got) ->
			let (_, td) =
				try SMap.find n sem.ss_types
				with Not_found -> Errors.type_error ty.tyloc
					(Unbound {typ="type";name=n})
			in
			begin match td with
			| TDUnspec _ | TDVariant _ | TDRecord _ ->
					ret @@ UserType ((n, None), List.map (unalias_rec sem) ta_got)
			| TDAlias _ ->
					unalias_rec sem (unalias sem ty)
			end
	| Arrow (i, o) -> ret @@ Arrow (unalias_rec sem i, unalias_rec sem o)
	| Product tyl -> ret @@ Product (List.map (unalias_rec sem) tyl)
	end


let rec print_witness ?(secure=false) self sem (ty:typ): string =
	begin match (unalias sem ty).tydesc with
	| Variable a -> a
	| UserType ((n, _), []) ->
			String.capitalize_ascii n
	| UserType ((n, _), [ty]) ->
			sprintf "%s %s" (String.capitalize_ascii n)
			(print_witness ~secure:true self sem ty)
			|> if secure then sprintf "(%s)" else Fun.id
	| UserType ((n, _), ta) ->
			let args =
				List.map (print_witness self sem) ta |> String.concat ", "
			in
			sprintf "%s (%s)" (String.capitalize_ascii n)
			args
			|> if secure then sprintf "(%s)" else Fun.id
	| Arrow (tyi, tyo) -> 
	   sprintf "Arrow (%s, %s)"
 						(print_witness self sem tyi)
 						(print_witness self sem tyo)
	   |> if secure then sprintf "(%s)" else Fun.id
	| Product [] -> "Unit"
	| Product [_] -> assert false
	| Product tyl ->
			List.map (print_witness self sem) tyl
			|> String.concat ", "
			|> sprintf "Tuple%d (%s)" (List.length tyl)
			|> if secure then sprintf "(%s)" else Fun.id
	end

let field_type self sem fl: string =
	let print_one_field f =
		let args =
			begin match f.fs_type_args with
			| [] -> ""
			| ta -> String.concat ", " ta |> sprintf " (%s)"
			end
		in
		sprintf "| F%s%s -> %s"
		f.fs_name args (print_witness self sem f.fs_field_type)
	in
	List.map print_one_field fl
		|> String.concat "\n\t\t\t"
		|> if_empty "| _ -> ."

let untuple_type max has_unspec =
	let nth i =
		let types = list_init 1 i (sprintf "a%d") |> String.concat " " in
		let args = list_init 1 i (sprintf "a%d") |> String.concat " * " in
		let res = list_init 1 i (sprintf "a%d witness") |> String.concat " * " in
		let tuple = list_init 1 i (sprintf "ty%d") |> String.concat ", " in
		let assert_false ="\n\t\t\t| _ -> assert false" in
		sprintf
		"let untuple%d_type: type %s. (%s) witness\n\t\t\t-> %s=\n\
      \t\t\tbegin function\n\
      \t\t\t| Tuple%d (%s) -> (%s)%s\n\
      \t\t\tend\n\n\t\t" i types args res i tuple tuple
			(if has_unspec then assert_false else "")
	in
	list_init 2 max nth |> String.concat ""

let spec_type self sem =
	let print_one_term (n, ta, ty) =
		let args =
			if ta = [] then "" else
			if List.compare_length_with ta 1 = 0 then " " ^ List.hd ta else
			String.concat ", " ta |> sprintf " (%s)"
		in
		sprintf "| S_%s%s -> %s" n args (print_witness self sem ty)
	in
	SMap.bindings sem.ss_terms |>
	List.filter_map (fun (n, (_, (ta, ty, o))) ->
		Option.map (Fun.const (n, ta, ty)) o) |>
	List.map print_one_term
	|> String.concat "\n\t\t\t"
	|> if_empty "| _ -> ."

let unspec_type self sem =
	let print_one_term (n, ta, ty) =
		let args =
			if ta = [] then "" else
			if List.compare_length_with ta 1 = 0 then " " ^ List.hd ta else
			String.concat ", " ta |> sprintf " (%s)"
		in
		sprintf "| U_%s%s -> %s" n args (print_witness self sem ty)
	in
	SMap.bindings sem.ss_terms |>
	List.filter_map (fun (n, (_, (ta, ty, o))) ->
		Option.fold ~some:(Fun.const None) ~none:(Some (n, ta, ty)) o) |>
	List.map print_one_term
	|> String.concat "\n\t\t\t"
	|> if_empty "| _ -> ."

let unspec_compute sem =
	let print_one_term (n, ta) =
		let args = if ta = [] then "" else " _" in
		sprintf "| U_%s%s -> P.%s" n args n
	in
	SMap.bindings sem.ss_terms |>
	List.filter_map (fun (n, (_, (ta, _, o))) ->
		Option.fold ~some:(Fun.const None) ~none:(Some (n, ta)) o) |>
	List.map print_one_term
	|> String.concat "\n\t\t\t"
	|> if_empty "| _ -> ."

let type_string_of_wit sem =
	let one_type (s, (_, td)) =
		let ta, alias =
			begin match td with
			| TDUnspec i -> List.init i (sprintf "a%d"), false
			| TDVariant (ta, _)
			| TDRecord (ta, _) -> ta, false
			| TDAlias (ta, _) -> ta, true
			end
		in
		let args =
			begin match ta with
			| [] -> ""
			| [a] -> sprintf " %s" a
			| _ -> String.concat ", " ta |> sprintf " (%s)"
			end
		in
		let percentss =  List.map (Fun.const "%s") ta |> String.concat ", " in
		let sprintf_args =
			List.map (sprintf " (string_of_wit %s)") ta |> String.concat ""
		in
		let constr = String.capitalize_ascii s in
		if alias then [] else (fun x -> [x])
		begin match ta with
		| [] -> sprintf "| %s -> \"%s\"" constr s
		| _  -> sprintf "| %s%s -> Format.sprintf \"%s(%s)\"%s" constr args s
		percentss sprintf_args
		end
	in
	SMap.bindings sem.ss_types
	|> List.concat_map one_type
	|> String.concat "\n\t\t\t"

let print_val_type sem =
	let one_type (s, (_, td)) =
		let ta, alias =
			begin match td with
			| TDUnspec 0 -> [], false
			| TDUnspec i -> List.init i (sprintf "a%d"), false
			| TDVariant (ta, _) | TDRecord (ta, _) -> ta, false
			| TDAlias (ta, _) -> ta, true
			end
		in
		let args =
			begin match ta with
			| [] -> ""
			| [a] -> sprintf " %s" a
			| _ -> String.concat ", " ta |> sprintf " (%s)"
			end
		in
		let print_args =
			List.map (sprintf " (print_val %s)") ta
			|> String.concat ""
		in
		if alias then [] else
		[sprintf "| %s%s -> %s%s v"
			(String.capitalize_ascii s) args (print_ sem s) print_args]
	in
	SMap.bindings sem.ss_types
	|> List.concat_map one_type
	|> String.concat "\n\t\t\t"

let print_constr cl: string =
	let print_one_constr c =
		let args =
			begin match c.cs_type_args with
			| [] -> ""
			| _ -> " _"
			end
		in
		sprintf "| C%s%s -> \"%s\""
		c.cs_name args c.cs_name
	in
	List.map print_one_constr cl
		|> String.concat "\n\t\t\t"
		|> if_empty "| _ -> ."

let print_field fl: string =
	let print_one_field f =
		let args =
			begin match f.fs_type_args with
			| [] -> ""
			| _ -> " _"
			end
		in
		sprintf "| F%s%s -> \"%s\""
		f.fs_name args f.fs_name
	in
	List.map print_one_field fl
		|> String.concat "\n\t\t\t"
		|> if_empty "| _ -> ."

let print_spec sem =
	let print_one_term (n, ta) =
		let args = if ta = [] then "" else " _" in
		sprintf "| S_%s%s -> \"%s\"" n args n
	in
	SMap.bindings sem.ss_terms |>
	List.filter_map (fun (n, (_, (ta, _, o))) ->
		Option.map (Fun.const (n, ta)) o) |>
	List.map print_one_term
	|> String.concat "\n\t\t\t"
	|> if_empty "| _ -> ."

let print_unspec sem =
	let print_one_term (n, ta) =
		let args = if ta = [] then "" else " _" in
		sprintf "| U_%s%s -> \"%s\"" n args n
	in
	SMap.bindings sem.ss_terms |>
	List.filter_map (fun (n, (_, (ta, _, o))) ->
		Option.fold ~some:(Fun.const None) ~none:(Some (n, ta)) o) |>
	List.map print_one_term
	|> String.concat "\n\t\t\t"
	|> if_empty "| _ -> ."

let get_eq_type sem =
	let one_type (s, (_, td)): string list =
		let ta, alias =
			begin match td with
			| TDUnspec i -> List.init i (sprintf "ty%d"), false
			| TDVariant (ta, _) | TDRecord (ta, _) -> ta, false
			| TDAlias (ta, _) -> ta, true
			end
		in
		if alias then [] else
		let c = String.capitalize_ascii s in
		if ta = [] then [sprintf "| %s, %s -> Refl" c c] else
		let args x =
			begin match ta with
			| [] -> ""
			| [a] -> sprintf " %s%s" x a
			| _ ->
					List.map (sprintf "%s%s" x) ta
					|> String.concat ", "
					|> sprintf " (%s)"
			end
		in
		let leteq a =
			sprintf "let eq%s = geteq a%s b%s in" a a a
		in
		let leteqs = List.map leteq ta |> String.concat "\n\t\t\t\t\t\t" in
		let eqs = List.map (sprintf "eq%s") ta |> String.concat ", " in
		let refls = List.map (Fun.const "Refl") ta |> String.concat ", " in
		[sprintf "\
			| %s%s, %s%s ->\n\t\t\t\t\t\t\
				%s\n\t\t\t\t\t\t\
				begin match %s with %s -> Refl end"
			c (args "a") c (args "b") leteqs eqs refls]
	in
	SMap.bindings sem.ss_types
	|> List.concat_map one_type
	|> String.concat "\n\t\t\t\t"

let spec_compute self sem =
	let print_constructor (((c, _), (_, ta)):constructor) =
		begin match ta with
		| []  -> sprintf "C%s" c
		| [a] ->
				sprintf "C%s %s" c (print_witness ~secure:true self sem a)
		| _   -> sprintf "C%s (%s)" c
			(List.map (print_witness self sem) ta |> String.concat ", ")
		end
	in
	let print_field (((f, _), (_, ta)):field) =
		begin match ta with
		| []  -> sprintf "F%s" f
		| [a] -> sprintf "F%s %s" f (print_witness ~secure:true self sem a)
		| _   -> sprintf "F%s (%s)" f
			(List.map (print_witness self sem) ta |> String.concat ", ")
		end
	in
	let rec print_pattern p =
		begin match p.pdesc with
		| PType (p, _) -> print_pattern p
		| PVar x -> sprintf "PVar (\"%s\", %s)" x (print_witness self sem p.ptyp)
		| PWild -> sprintf "PWild %s" (print_witness ~secure:true self sem p.ptyp)
		| PConstr (c, p) ->
				sprintf "PConstr (%s, %s)" (print_constructor c) (print_pattern p)
		| PTuple [] -> "Ptt"
		| PTuple pl -> sprintf "PTuple%d (%s)"
				(List.length pl)
				(List.map print_pattern pl |> String.concat ", ")
		| POr (p1, p2) ->
				sprintf "POr (%s, %s)" (print_pattern p1) (print_pattern p2)
		| PRecord fpl ->
				sprintf "PRecord (%s, %s)"
				(print_witness self sem p.ptyp)
				(print_pattern_record fpl)
		end
	and print_pattern_record =
		begin function
		| [] -> "PNil"
		| (f, p) :: fpl ->
				sprintf "PCons (%s, %s, %s)"
				(print_field f) (print_pattern p) (print_pattern_record fpl)
		end
	in
	let rec print_term t =
		begin match t.tdesc with
		| TVar (LetBound (x, ty)) -> sprintf "Var (\"%s\", %s)" x (print_witness self sem ty)
		| TVar (TopLevel (Specified, (x, _), ta, _)) ->
				begin match ta with
				| [] -> sprintf "Spec S_%s" x
				| _ ->
						sprintf "Spec (S_%s (%s))" x
						(List.map (print_witness self sem) ta |> String.concat ", ")
				end
		| TVar (TopLevel (Nonspecified, (x, _), ta, _)) ->
				begin match ta with
				| [] -> sprintf "Unspec U_%s" x
				| _ ->
						sprintf "Unspec (U_%s (%s))" x
						(List.map (print_witness self sem) ta |> String.concat ", ")
				end
		| TConstr (c, t) ->
				sprintf "Constr (%s, %s)" (print_constructor c) (print_term t)
		| TTuple [] -> "Tt"
		| TTuple [_] -> assert false
		| TTuple tl ->
				sprintf "Tuple%d (%s)" (List.length tl)
				(List.map print_term tl |> String.concat ", ")
		| TFunc (p, sk) -> sprintf "Func (%s, %s)"
			(print_pattern p) (print_skel sk)
		| TRecSet (t, ftl) ->
				sprintf "RecSet (%s, %s)"
				(print_term t) (print_term_record ftl)
		| TRecMake ftl ->
				sprintf "RecMake (%s, %s)"
				(print_witness self sem t.ttyp) (print_term_record ftl)
		| TField (t, f) ->
				sprintf "Field (%s, %s)" (print_term t) (print_field f)
		| TNth _ -> assert false (* TODO *)
		end
	and print_term_record =
		begin function
		| [] -> "TNil"
		| (f, t) :: ftl ->
				sprintf "TCons (%s, %s, %s)"
				(print_field f) (print_term t) (print_term_record ftl)
		end
	and print_skel s =
		begin match s.sdesc with
		| Return t -> sprintf "Return (%s)" (print_term t)
		| Branching (ty, bl) -> sprintf "Branching (%s, [%s])"
			(print_witness self sem ty) (List.map print_skel bl |> String.concat "; ")
		| LetIn (NoBind, p, s1, s2) -> sprintf "LetIn (%s, %s, %s)"
			(print_pattern p) (print_skel s1) (print_skel s2)
		| LetIn ( (TermBind (k, b, ta, b_typ) | SymbolBind (_, k, b, ta, b_typ)),
							p, s1, s2) ->
			(* LATER: do better? *)
				let b1, b2, b3 =
					begin match (unalias_rec sem b_typ).tydesc with
					| Arrow ({tydesc=Product [a; b];_}, c) -> a, b, c
					| _ -> assert false
					end
				in
			let mkty tydesc = {tydesc;tyloc=ghost_loc} in
			let new_skel =
				{ styp = s.styp ;
					sloc=ghost_loc ;
					sdesc =
						LetIn (NoBind, {ptyp=s1.styp;pdesc=PVar "_tmp";ploc=ghost_loc}, s1,
						{ styp = b3; 
							sloc = ghost_loc ;
							sdesc =
							Apply (
								{ ttyp = b_typ;
									tloc = ghost_loc ;
									tdesc =
									TVar (TopLevel (k, b, ta, b_typ)) },
								{ ttyp = mkty @@ Product [b1;b2] ;
									tloc = ghost_loc ;
									tdesc =
									TTuple [
									{ ttyp = b1;
										tloc = ghost_loc ;
										tdesc =
										TVar (LetBound ("_tmp", s1.styp))};
									{ ttyp = b2;
										tloc = ghost_loc ;
										tdesc =
										TFunc (p, s2)}] })})}
			in print_skel new_skel
		| Apply (f, x) -> sprintf "Apply (%s, %s)"
			(print_term f) (print_term x)
		| Exists _ -> assert false (* TODO *)
		| Match (s, psl) -> sprintf "Match (%s, [%s])"
				(print_skel s)
				(List.map (fun (p,s) -> sprintf "(%s, %s)"
				(print_pattern p) (print_skel s)) psl |> String.concat "; ")
		end
	in
	let print_one_term (n, ta, t) =
		let args =
			if ta = [] then "" else
			if List.compare_length_with ta 1 = 0 then " " ^ List.hd ta else
			String.concat ", " ta |> sprintf " (%s)"
		in
		sprintf "| S_%s%s -> %s" n args (print_term t)
	in
	SMap.bindings sem.ss_terms |>
	List.filter_map (fun (n, (_, (ta, _, o))) ->
		Option.map (fun t -> (n, ta, t)) o) |>
	List.map print_one_term
	|> String.concat "\n\t\t\t"
	|> if_empty "| _ -> ."

let get_field_value fl: string =
	let print_one_field f =
		let args = if f.fs_type_args = [] then "" else " _" in
		sprintf "| F%s%s, VCons (F%s%s, v, _) -> v"
		f.fs_name args f.fs_name args
	in
	List.map print_one_field fl
		|> String.concat "\n\t\t\t"

let record_compute sem: string =
	let print_one_type n def =
		begin match def with
		| TDRecord (ta, fl) ->
				let args =
					begin match ta with
					| [] -> ""
					| [a] -> sprintf " %s" a
					| ta -> String.concat ", " ta |> sprintf " (%s)"
					end
				in
				let fields =
					List.map (fun f ->
						sprintf "%s=from_typed_value @@@@ get_field_value (F%s%s) fvl"
						f.fs_name f.fs_name args) fl
					|> String.concat ";\n\t\t\t\t\t"
				in
				sprintf "| %s%s -> Value ({\n\t\t\t\t\t%s})"
					(String.capitalize_ascii n) args
					fields
		| _ -> ""
		end
	in
	List.map (fun (n, (_, def)) ->
		print_one_type n def) (SMap.bindings sem.ss_types)
	|> List.filter ((<>)"")
	|> String.concat "\n\t\t\t\t "

let field_reset sem: string =
	let print_one_field isalone f =
			let args = if f.fs_type_args = [] then "" else " _" in
		if not isalone then
			sprintf "| F%s%s -> Value {(from_typed_value v) with %s=from_typed_value w}"
			f.fs_name args f.fs_name
		else
			sprintf "| F%s%s -> Value {%s=from_typed_value w}"
			f.fs_name args f.fs_name
	in
	let print_one_type (_, _, fl) =
		begin match fl with
		| [] -> assert false
		| [f] -> print_one_field true f
		| _-> List.map (print_one_field false) fl |> String.concat "\n\t\t\t"
		end
	in
	let fl = Skeleton.get_fields_by_type sem in
	List.map print_one_type fl
	|> String.concat "\n\t\t\t"
	|> if_empty "| _ -> ."











(********** Tuple stuff **********)

(* Compute max tuple arity *)
let max_tuple sem: int =
	let rec max_tuple_type (ty:typ): int =
		begin match ty.tydesc with
		| Variable _ -> 0
		| UserType (_, ta) ->
				max_list 0 (List.map max_tuple_type ta)
		| Arrow (i, o) -> max (max_tuple_type i) (max_tuple_type o)
		| Product tl ->
				max_list (List.length tl) (List.map max_tuple_type tl)
		end
	in
	let rec max_tuple_pat (p:pattern): int =
		begin match p.pdesc with
		| PWild -> max_tuple_type p.ptyp
		| PVar _ -> max_tuple_type p.ptyp
		| PConstr ((_, (_, ta)), p) ->
				max_list (max_tuple_pat p) (List.map max_tuple_type ta)
		| PTuple pl -> List.fold_left (fun m p ->
				max (max_tuple_pat p) m) (List.length pl) pl
		| PRecord fpl ->
				List.fold_left (fun m ((_, (_, ta)), p) ->
					let max_m_ta =
						max_list m (List.map max_tuple_type ta)
					in
					max max_m_ta (max_tuple_pat p)) 0 fpl
		| POr (p1, p2) -> max (max_tuple_pat p1) (max_tuple_pat p2)
		| PType (p, _) -> max_tuple_pat p
		end
	and max_tuple_term (t:term): int =
		begin match t.tdesc with
		| TVar _ -> 0
		| TConstr ((_, (_, ta)), t) ->
				max_list (max_tuple_term t) (List.map max_tuple_type ta)
		| TTuple tl ->
				max_list (List.length tl) (List.map max_tuple_term tl)
		| TFunc (p, s) -> max (max_tuple_pat p) (max_tuple_skel s)
		| TField (t, (_, (_, ta))) ->
				max_list (max_tuple_term t) (List.map max_tuple_type ta)
		| TRecMake ftl -> List.fold_left (fun m ((_, (_, ta)), t) ->
				let max_m_ta = max_list m (List.map max_tuple_type ta) in
				max max_m_ta (max_tuple_term t)) 0 ftl
		| TRecSet (t, ftl) -> List.fold_left (fun m ((_, (_, ta)), t) ->
				let max_m_ta = max_list m (List.map max_tuple_type ta) in
				max max_m_ta (max_tuple_term t)) (max_tuple_term t) ftl
		| TNth _ -> assert false (* TODO *)
		end
	and max_tuple_skel (s:skeleton): int =
		begin match s.sdesc with
		| Branching (ty, sl) -> List.fold_left (fun m s ->
				max m @@ max_tuple_skel s) (max_tuple_type ty) sl
		| Match (s, psl) ->
				max_list (max_tuple_skel s)
				(List.map (fun (p, s) -> max (max_tuple_pat p) (max_tuple_skel s)) psl)
		| LetIn (b, p, s, s') ->
				let m =
					max (max_tuple_pat p) (max (max_tuple_skel s) (max_tuple_skel s'))
				in
				begin match b with
				| NoBind -> m
				| _ -> max 2 m
				end
		| Exists _ -> 0
		| Return t -> max_tuple_term t
		| Apply (f, x) -> max (max_tuple_term f) (max_tuple_term x)
		end
	in
	let max_tuple_term_decl (_, (_, ty, o)) =
		begin match o with
		| None -> (max_tuple_type ty)
		| Some t -> max (max_tuple_term t) (max_tuple_type ty)
		end
	in
	let max_tuple_type_decl (_, td) =
		begin match td with
		| TDUnspec _ -> 0
		| TDVariant (_, cl) ->
				max_list 0 (List.map (fun c ->
					max_tuple_type c.cs_input_type) cl)
		| TDRecord (_, fl) ->
				max_list 0 (List.map (fun f ->
					max_tuple_type f.fs_field_type) fl)
		| TDAlias (_, ty) ->
				max_tuple_type ty
		end
	in
	SMap.fold (fun _ term i ->
		max i (max_tuple_term_decl term)) sem.ss_terms @@
	SMap.fold (fun _ typ i ->
		max i (max_tuple_type_decl typ)) sem.ss_types @@
	0

let tuple_witness max =
	if max < 2 then "" else
		list_init 2 max (fun i ->
			let args = list_init 1 i (sprintf "'a%d witness") |> String.concat " * " in
			let prod = list_init 1 i (sprintf "'a%d") |> String.concat " * " in
			let result = sprintf "(%s) witness" prod in
			sprintf "| Tuple%d: %s -> %s" i args result)
		|> String.concat "\n\t\t"

let tuple_string_of_wit max =
	let tuple i =
		let args =
			List.init i (sprintf "a%d") |> String.concat ", " |> sprintf "(%s)" in
		let percentss =
			List.init i (Fun.const "%s") |> String.concat ", " |> sprintf "(%s)" in
		let sprintf_args =
			List.init i (sprintf "(string_of_wit a%d)") |> String.concat " " in
		sprintf
			"| Tuple%d %s ->\n\t\t\t\tFormat.sprintf \"%s\"\n\t\t\t\t%s"
			i args percentss sprintf_args
	in
	list_init 2 max tuple |> String.concat "\n\t\t\t"

let print_val_tuple max =
	let tuple i =
		let args x =
			List.init i (sprintf "%s%d" x) |> String.concat ", " |> sprintf "(%s)" in
		let percentss =
			List.init i (Fun.const "%s") |> String.concat ", " |> sprintf "(%s)" in
		let sprintf_args =
			List.init i (fun i -> sprintf "(print_val ty%d v%d)" i i)
			|> String.concat " "
		in
		sprintf "\
			| Tuple%d %s ->\n\t\t\t\t\
				let %s = v in\n\t\t\t\t\
				Format.sprintf \"%s\"\n\t\t\t\t\
				%s"
			i (args "ty") (args "v") percentss sprintf_args
	in
	list_init 2 max tuple |> String.concat "\n\t\t\t"

let get_eq_tuple max =
	let tuple i =
		let args x =
			List.init i (sprintf "%s%d" x) |> String.concat ", " |> sprintf "(%s)" in
		let leteq k =
			sprintf "let eq%d = geteq tya%d tyb%d in" k k k
		in
		let leteqs = List.init i leteq |> String.concat "\n\t\t\t\t\t\t" in
		let eqs = List.init i (sprintf "eq%d") |> String.concat ", " in
		let refls = List.init i (Fun.const "Refl") |> String.concat ", " in
		sprintf "\
			| Tuple%d %s, Tuple%d %s ->\n\t\t\t\t\t\t\
				%s\n\t\t\t\t\t\t\
				begin match %s with %s -> Refl end"
			i (args "tya") i (args "tyb") leteqs eqs refls
	in
	list_init 2 max tuple |> String.concat "\n\t\t\t\t"

let typed_value_tuple max =
	let tuple i =
		let constr_args =
			list_init 1 i (sprintf "'a%d typed_value") |> String.concat " * " in
		let type_args =
			list_init 1 i (sprintf "'a%d") |> String.concat " * " in
		sprintf "\n\t\t| VTuple%d: %s -> (%s) typed_value" i constr_args type_args
	in
	list_init 2 max tuple |> String.concat ""

let pattern_tuple max =
	let tuple i =
		let args = List.init i (sprintf "'a%d pattern") |> String.concat " * " in
		let ta =
			List.init i (sprintf "'a%d")
			|> String.concat " * "
			|> sprintf "(%s) pattern"
		in
		sprintf "| PTuple%d: %s -> %s"
			i args ta
	in
	list_init 2 max tuple |> String.concat "\n\t\t"

let term_tuple max =
	let tuple i =
		let args = List.init i (sprintf "'a%d term") |> String.concat " * " in
		let ta =
			List.init i (sprintf "'a%d")
			|> String.concat " * "
			|> sprintf "(%s) term"
		in
		sprintf "| Tuple%d: %s -> %s"
			i args ta
	in
	list_init 2 max tuple |> String.concat "\n\t\t"

let term_nth max =
	let nth i =
		let nth_i j =
			let arg =
				list_init 1 i (sprintf "'a%d")
				|> String.concat " * "
				|> sprintf "(%s) term"
			in
			let res = sprintf "'a%d term" j in
			sprintf "| Nth%d_%d: %s -> %s"
				i j arg res
		in
		list_init 1 i nth_i |> String.concat "\n\t\t"
	in
	list_init 2 max nth |> String.concat "\n\t\t"

let pattern_type_tuple max =
	let tuple i =
		let args x =
			List.init i (sprintf "%s%d" x)
			|> String.concat ", "
			|> sprintf "(%s)"
		in
		sprintf "| PTuple%d %s -> Tuple%d %s"
			i (args "p") i (args "pattern_type p")
	in
	list_init 2 max tuple |> String.concat "\n\t\t\t"

let term_type_tuple max =
	let tuple i =
		let args x =
			List.init i (sprintf "%s%d" x)
			|> String.concat ", "
			|> sprintf "(%s)"
		in
		sprintf "| Tuple%d %s -> Tuple%d %s"
			i (args "t") i (args "term_type t")
	in
	list_init 2 max tuple |> String.concat "\n\t\t\t"

let term_type_nth max =
	let nth i =
		let nth_i j =
			let args =
				list_init 1 i (fun x -> if x = j then "ty" else "_")
				|> String.concat ", "
				|> sprintf "(%s)"
			in
			sprintf "| Nth%d_%d t ->\n\t\t\t\t\
			let %s = untuple%d_type (term_type t) in ty"
				i j args i
		in
		list_init 1 i nth_i |> String.concat "\n\t\t\t"
	in
	list_init 2 max nth |> String.concat "\n\t\t\t"

let extend_env_tuple max =
	let tuple i =
		let args x =
			List.init i (sprintf "%s%d" x)
			|> String.concat ", "
			|> sprintf "(%s)"
		in
		let extend k =
			let kminus1 = if k = 0 then "" else string_of_int (k-1) in
			sprintf "let* e%d = extend_env e%s p%d v%d in" k kminus1 k k
		in
		sprintf "\
			| PTuple%d %s ->\n\t\t\t\t\
			let %s = untuple%d v in\n\t\t\t\t\
			let (let*) = Option.bind in\n\t\t\t\t\
			%s\n\t\t\t\t\
			Some e%d"
			i (args "p") (args "v") i (List.init i extend |> String.concat "\n\t\t\t\t")
			(i-1)
	in
	list_init 2 max tuple |> String.concat "\n\t\t\t"

let from_typed_value_tuple max =
	let tuple i =
		let constr_args =
			list_init 1 i (sprintf "v%d") |> String.concat ", "
		in
		let component =
			list_init 1 i (sprintf "from_typed_value v%d") |> String.concat ", "
		in
		sprintf "\n\t\t\t| VTuple%d (%s) -> (%s)" i constr_args component
	in
	list_init 2 max tuple |> String.concat ""

let untuple_func max =
	let tuple i =
		let type_args = list_init 1 i (sprintf "a%d") |> String.concat " " in
		let args = list_init 1 i (sprintf "v%d") |> String.concat ", " in
		let prod_ai = list_init 1 i (sprintf "a%d") |> String.concat " * " in
		let ret_typ =
			list_init 1 i (sprintf "a%d typed_value") |> String.concat " * " in
		let vals = list_init 1 i (sprintf "a%d") |> String.concat ", " in
		let ret_val = list_init 1 i (sprintf "Value a%d") |> String.concat ", " in
		sprintf "\
			and untuple%d: type %s.\n\t\t\t\
				(%s) typed_value ->\n\t\t\t\t\
					%s =\n\t\t\t\
				begin function\n\t\t\t\
				| VTuple%d (%s) -> (%s)\n\t\t\t\
				| v ->\n\t\t\t\t\
						let (%s) = from_typed_value v in\n\t\t\t\t\
						(%s)\n\t\t\t\
				end"
			i type_args prod_ai ret_typ i args args vals ret_val
	in
	list_init 2 max tuple
	|> String.concat "\n\n\t\t"

let print_typed_value_tuple max =
	let tuple i =
		let args = list_init 1 i (sprintf "v%d") |> String.concat ", " in
		let types = list_init 1 i (sprintf "ty%d") |> String.concat ", " in
		let percentss = list_init 1 i (Fun.const "%s") |> String.concat ", " in
		let print_args =
			list_init 1 i (fun k ->
				sprintf "(print_typed_value ty%d v%d)" k k) |> String.concat " " in
		sprintf "\
					| VTuple%d (%s) ->\n\
      \t\t\t\tbegin match w with\n\
      \t\t\t\t| Tuple%d (%s) ->\n\
      \t\t\t\t\tFormat.sprintf \"(%s)\"\n\
      \t\t\t\t\t%s\n\
      \t\t\t\t| _ -> failwith \"Impossible: printing tuple%d\"\n\
      \t\t\t\tend"
				i args i types percentss print_args i
	in
	list_init 2 max tuple
	|> String.concat "\n\t\t\t"

let eval_term_tuple max =
	let tuple i =
		let args x =
			List.init i (sprintf "%s%d" x)
			|> String.concat ", "
			|> sprintf "(%s)"
		in
		sprintf "\
			| Tuple%d %s -> Value (\n\t\t\t\t\
			%s)"
			i (args "t")
			(List.init i (sprintf "from_typed_value (eval_term e t%d)")
				|> String.concat ",\n\t\t\t\t")
	in
	list_init 2 max tuple |> String.concat "\n\t\t\t"

let eval_term_nth max =
	let nth i =
		let nth_i j =
			let tuple =
				list_init 1 i (fun x -> if x = j then "v" else "_")
				|> String.concat ", "
				|> sprintf "(%s)"
			in
			sprintf "| Nth%d_%d t ->\n\t\t\t\t\
				let %s = untuple%d (eval_term e t) in v"
				i j tuple i
		in
		list_init 1 i nth_i |> String.concat "\n\t\t\t"
	in
	list_init 2 max nth |> String.concat "\n\t\t\t"

let print_term_tuple max =
	let tuple i =
		let args x =
			List.init i (sprintf "%s%d" x)
			|> String.concat ", "
			|> sprintf "(%s)"
		in
		let percentss = List.init i (Fun.const "%s") |> String.concat ", " in
		sprintf "\
			| Tuple%d %s ->\n\t\t\t\t\
			Format.sprintf \"(%s)\"\n\t\t\t\t\
			%s"
			i (args "t") percentss
			(List.init i (sprintf "(print_term ~indent ~protect t%d)") |> String.concat " ")
	in
	list_init 2 max tuple |> String.concat "\n\t\t\t"

let print_term_nth max =
	let nth i =
		let nth_i j =
			sprintf "| Nth%d_%d t ->\n\t\t\t\t\t\
				Format.sprintf \"%%s.%d\" (print_term ~indent ~protect t)"
				i j j
		in
		list_init 1 i nth_i |> String.concat "\n\t\t\t"
	in
	list_init 2 max nth |> String.concat "\n\t\t\t"
let print_pattern_tuple max =
	let tuple i =
		let args x =
			List.init i (sprintf "%s%d" x)
			|> String.concat ", "
			|> sprintf "(%s)"
		in
		let percentss = List.init i (Fun.const "%s") |> String.concat ", " in
		sprintf "\
			| PTuple%d %s ->\n\t\t\t\t\
			Format.sprintf \"(%s)\"\n\t\t\t\t\
			%s"
			i (args "p") percentss
			(List.init i (sprintf "(print_pattern p%d)") |> String.concat " ")
	in
	list_init 2 max tuple |> String.concat "\n\t\t\t"

let tuple_cont max =
	let tuple i =
		let tuple i j =
			let args =
				let before_hole =
					List.init (j-1) (fun k ->
						sprintf "'a%d typed_value * 'a%d witness" k k)
					in
				let after_hole =
					if i = j then [] else
						"env" ::
							List.init (i-j) (fun k -> let k = k + j in
							sprintf "'a%d term" k)
				in
				before_hole @ after_hole
			in
			let prod = List.init i (sprintf "'a%d") |> String.concat " * " in
			let typ = String.concat "\n\t\t\t\t* " args in
			sprintf "| Ktuple%d_%d:\n\t\t\t\t%s\n\t\t\t\t\
					* (%s, 'b) cont\n\t\t\t\t\t-> ('a%d, 'b) cont" i j typ prod (j-1)
		in
		List.init i (fun j -> tuple i (j+1)) |> String.concat "\n\t\t\t"
	in
	list_init 2 max tuple |> String.concat "\n\t\t\t"

let nth_cont max =
	let nth i =
		let nth_i j =
			let arg = sprintf "('a%d, 'b) cont" j in
			let res =
				list_init 1 i (sprintf "'a%d")
				|> String.concat " * "
				|> sprintf "(%s, 'b) cont"
			in
			sprintf "| Knth%d_%d:\n\t\t\t\t%s -> %s"
				i j arg res
		in
		list_init 1 i nth_i |> String.concat "\n\t\t\t"
	in
	list_init 2 max nth |> String.concat "\n\t\t\t"

let tuple_string_of_cont max =
	let tuple i =
		let tuple i j =
			let args, print =
				let before_hole_arg =
					list_init 1 (j-1) (fun k ->
						sprintf "v%d, ty%d" k k)
					in
				let before_hole_print =
					list_init 1 (j-1) (fun k ->
						sprintf "(print_typed_value ty%d v%d)" k k)
					in
				let after_hole_arg =
					if i = j then [] else
						"_" ::
							list_init (j+1) i (sprintf "t%d")
				in
				let after_hole_print =
					list_init (j+1) i (sprintf "(print_term t%d)")
				in
				before_hole_arg @ after_hole_arg,
				before_hole_print @ "\"□\"" :: after_hole_print
			in
			let percentss =
				list_init 1 i (Fun.const "%s") |> String.concat ","
			in
			sprintf "\
			| Ktuple%d_%d (%s, k) -> Format.sprintf\n\t\t\t\t\
        \"- (%s)\\n%%s\"\n\t\t\t\t\
				%s\n\t\t\t\t(string_of_cont k)"
				i j (String.concat ", " args) percentss (String.concat " " print)
		in
		list_init 1 i (tuple i) |> String.concat "\n\t\t\t"
	in
	list_init 2 max tuple |> String.concat "\n\t\t\t"

let nth_string_of_cont max =
	let nth i =
		let nth_i j =
			sprintf "| Knth%d_%d k -> sprintf\n\t\t\t\t\
        \"- □.%d\\n%%s\" (string_of_cont k)" i j j
		in
		list_init 1 i nth_i |> String.concat "\n\t\t\t"
	in
	list_init 2 max nth |> String.concat "\n\t\t\t"

let term_step_tuple max =
	let tuple i =
		let args x =
			List.init i (sprintf "%s%d" x)
			|> String.concat ", "
			|> sprintf "(%s)"
		in
		let args_rest =
			List.init i (sprintf "t%d")
			|> List.tl
			|> String.concat ", "
		in
		sprintf "| Tuple%d %s -> Term (e, t0, Ktuple%d_1 (e, %s, k), fk)"
			i (args "t") i args_rest
	in
	list_init 2 max tuple |> String.concat "\n\t\t\t"

let term_step_nth max =
	let nth i =
		let nth_i j =
			sprintf "| Nth%d_%d t -> Term (e, t, Knth%d_%d k, fk)"
				i j i j
		in
		list_init 1 i nth_i |> String.concat "\n\t\t\t"
	in
	list_init 2 max nth |> String.concat "\n\t\t\t"

let cont_step_tuple max =
	let tuple i =
		let tuple i j =
			let before_hole =
				List.init (j-1) (fun k -> let k = k+1 in
					sprintf "v%d, ty%d" k k)
				in
			let after_hole =
				if i = j then [] else
					"e" ::
						List.init (i-j) (fun k -> let k = k + j + 1 in
						sprintf "t%d" k)
			in
			let after_after =
				if i <= j+1 then [] else
					"e" ::
						List.init (i-j-1) (fun k -> let k = k + j + 2 in
						sprintf "t%d" k)
			in
			if i <> j then
				sprintf "| Ktuple%d_%d (%s, k) ->\n\t\t\t\t\
					Term (e, t%d, Ktuple%d_%d (%s, k), fk)"
				i j
				(String.concat ", " (before_hole @ after_hole))
				(j+1) i (j+1)
				(String.concat ", " (before_hole@"v, ty"::after_after))
			else
				sprintf "| Ktuple%d_%d (%s, k) ->\n\t\t\t\t\
					Ret (VTuple%d (%s, v), Tuple%d (%s, ty), k, fk)"
				i j (String.concat ", " before_hole)
				i (list_init 1 (i-1) (sprintf "v%d") |> String.concat ", ")
				i (list_init 1 (i-1) (sprintf "ty%d") |> String.concat ", ")
		in
		list_init 1 i (tuple i) |> String.concat "\n\t\t\t"
	in
	list_init 2 max tuple |> String.concat "\n\t\t\t"

let cont_step_nth max =
	let nth i =
		let nth_i j =
			let args s =
				list_init 1 i (fun x -> if x = j then s else "_")
				|> String.concat ", "
				|> sprintf "(%s)"
			in
			sprintf "| Knth%d_%d k ->\n\t\t\t\t\
			let %s = untuple%d v in\n\t\t\t\t\
			let %s = untuple%d_type ty in\n\t\t\t\t\
			Ret (v, ty, k, fk)"
				i j (args "v") i (args "ty") i
		in
		list_init 1 i nth_i |> String.concat "\n\t\t\t"
	in
	list_init 2 max nth |> String.concat "\n\t\t\t"

let apply_spec_sig sem =
	let print_one_term (t, ty) =
		let rec get_typ t =
			begin match t.tydesc with
			| Arrow ({tydesc=Arrow _;_} as x, y) ->
					sprintf "(%s) -> %s" (print_type ~prefix:"P." x) (get_typ y)
			| Arrow (x, y) ->
					sprintf "%s -> %s" (print_type ~prefix:"P." x) (get_typ y)
			| _ -> sprintf "%s amstate" (print_type ~prefix:"P." t)
			end
		in
		sprintf "val apply_%s: %s" t (get_typ ty)
	in
	SMap.bindings sem.ss_terms |>
	List.filter_map (fun (n, (_, (ta, ty, o))) ->
		begin match o, ta with
		| None, _ -> None
		| _, _ :: _ -> None (* TODO: handle polymorphism *)
		| Some _, _ -> Some (n, ty)
		end) |>
	List.map print_one_term
	|> String.concat "\n\t\t"

let apply_spec self sem =
	let print_one_term (t, ty) =
		let rec rev_args ?(accu=[]) ty =
			begin match ty.tydesc with
			| Arrow (i, ty) -> rev_args ~accu:(i::accu) ty
			| _ -> accu
			end
		in
		let rev_args = rev_args ty in
		let a1an =
			list_init 1 (List.length rev_args) (sprintf " a%d") |> String.concat ""
		in
		let rec state rev_args =
			begin match rev_args with
			| [] -> sprintf "PSSpec S_%s" t
			| last :: rev_first_args ->
					let st = state rev_first_args in
					sprintf "PSApp (%s,\n\t\t\t\t\t%s, Value a%d)"
					st (print_witness self sem last) (List.length rev_args)
			end
		in
		let state = state rev_args in
		sprintf "let apply_%s%s =\n\
			\t\tlet state =\n\
			\t\t\tCSpec (\n\
			\t\t\t\t%s,\n\
			\t\t\t\tKid, None)\n\
			\t\tin {beg=state; cur=state; prev=[]; cpt=0}"
		t a1an state
	in
	SMap.bindings sem.ss_terms |>
	List.filter_map (fun (n, (_, (ta, ty, o))) ->
		begin match o, ta with
		| None, _ -> None
		| _, _ :: _ -> None (* TODO: handle polymorphism *)
		| Some _, _ -> Some (n, ty)
		end) |>
	List.map print_one_term
	|> String.concat "\n\t"

let spec_terms sem =
	SMap.bindings sem.ss_terms |>
	List.filter_map (fun (n, (_, (_, _, o))) ->
		begin match o with
		| None -> None
		| Some _ -> Some n
		end)
		|> List.map (sprintf "\"%s\"")
		|> String.concat "; "

