open Necro
open TypedAST
open Printing
open Util

(* replaces a line of the type C[{# name #}] where C is a context with C
 * applied to the term of name in the map *)
let search_and_replace map line =
	Str.global_substitute
		(Str.regexp "{# \\([a-zA-Z0-9_]*\\) #}")
		(fun s -> match SMap.find_opt (Str.matched_group 1 s) map with Some s -> s |
		None -> failwith (Str.matched_group 1 s))
		line

let special_comment =
	let version =
		begin match Build_info.V1.version () with
		| None -> "N/A"
		| Some v -> Build_info.V1.Version.to_string v
		end
	in
	"This file was automatically generated using necrodebug version " ^ version ^
	".\nSee https://gitlab.inria.fr/skeletons/necro-debug/ for more informations"

(* replaces a line of the type C[{# name #}] where C is a context with C
 * applied to the term of name in the map *)
let replace_special_comment line =
	Str.global_substitute
		(Str.regexp {|(\*\* \(\([^\*]\|\n\)*\) \*)|})
		(fun _ -> Format.sprintf "(** %s *)" special_comment)
		line

(* replaces a code of the type [{{x: code }}] by the refuting pattern [.], where
 [x] is a string given in argument *)
let search_and_refute x refute line =
	Str.global_substitute
		(Str.regexp ("{{" ^ x ^ {|:\(\([^}]\|\n\)*\)}}|}))
		(fun s ->
			if refute then " ." else
				Str.matched_group 1 s) line

(* replaces a code of the type [{{x_: code }}] by the anonymous pattern [_], where
 [x] is a string given in argument *)
let search_and_anonymize x refute line =
	Str.global_substitute
		(Str.regexp ("{{" ^ x ^ {|_:\(\([^}]\|\n\)*\)}}|}))
		(fun s ->
			if refute then "_" else
				Str.matched_group 1 s) line

(* remove a code of the type [{{xr: code }}] where
 [x] is a string given in argument *)
let search_and_remove x refute line =
	Str.global_substitute
		(Str.regexp ("{{" ^ x ^ {|r:\(\([^}]\|\n\)*\)}}|}))
		(fun s ->
			if refute then "" else
				Str.matched_group 1 s) line

(** [getlines ic] takes all the lines available in the input channel and puts
    them in a string. **)
let getlines ic =
	let rec f acc =
		try f ((input_line ic) :: acc) with End_of_file -> String.concat "\n" (List.rev acc)
	in f []



let gen self sem =
	let sem, _ = Skeleton.protect_ss reserved sem in
	let max_tuple = max_tuple sem in
	let constructors = Skeleton.get_all_constructors sem in
	let fields = Skeleton.get_all_fields sem in
	let all_unspec_types = sem.ss_types |> SMap.bindings
	|> List.filter_map (fun (_, (_, td) as v) ->
			match td with TDUnspec _ -> Some v | _ -> None)
	in
	let all_spec_types = sem.ss_types |> SMap.bindings
	|> List.filter_map (fun (_, (_, td) as v) ->
			match td with TDUnspec _ -> None | _ -> Some v)
	in
	let all_unspec_terms = sem.ss_terms |> SMap.bindings
	|> List.filter (fun (_, (_, (_, _, o))) -> Option.is_none o)
	in
	let all_spec_terms = sem.ss_terms |> SMap.bindings
	|> List.filter (fun (_, (_, (_, _, o))) -> Option.is_some o)
	in

	let smap_of_list l =
		List.fold_left (fun m (key, cont) -> SMap.add key cont m) SMap.empty l
	in
	let map =
		smap_of_list [
			("unspec_types", unspec_types sem) ;
			("spec_types", spec_types sem) ;
			("packing", packing sem (all_spec_types = [])) ;
			("unspec_terms", unspec_terms sem) ;
			("print_module_type", print_module_type false sem) ;
			("unspec_print_module_type", print_module_type true sem) ;
			("default_print", default_print sem) ;
			("spec_printers", spec_printers sem) ;
			("tuple_witness", tuple_witness max_tuple) ;
			("type_witness", type_witness sem) ;
			("constr", constr constructors) ;
			("field", field fields) ;
			("constr_type", constr_type constructors) ;
			("field_type", field_type self sem fields) ;
			("untuple_type", untuple_type max_tuple (all_unspec_types <> [])) ;
			("constr_compute", constr_compute constructors) ;
			("field_compute", field_compute fields) ;
			("remove_constr", remove_constr constructors) ;
			("spec", spec sem) ;
			("unspec", unspec sem) ;
			("spec_type", spec_type self sem) ;
			("unspec_type", unspec_type self sem) ;
			("unspec_compute", unspec_compute sem) ;
			("tuple_string_of_wit", tuple_string_of_wit max_tuple) ;
			("type_string_of_wit", type_string_of_wit sem) ;
			("print_val_tuple", print_val_tuple max_tuple) ;
			("print_val_type", print_val_type sem) ;
			("print_constr", print_constr constructors) ;
			("print_field", print_field fields) ;
			("print_spec", print_spec sem) ;
			("print_unspec", print_unspec sem) ;
			("get_eq_tuple", get_eq_tuple max_tuple) ;
			("get_eq_type", get_eq_type sem) ;
			("typed_value_tuple", typed_value_tuple max_tuple) ;
			("from_typed_value_tuple", from_typed_value_tuple max_tuple) ;
			("print_typed_value_tuple", print_typed_value_tuple max_tuple) ;
			("pattern_tuple", pattern_tuple max_tuple) ;
			("term_tuple", term_tuple max_tuple) ;
			("term_nth", term_nth max_tuple) ;
			("pattern_type_tuple", pattern_type_tuple max_tuple) ;
			("term_type_tuple", term_type_tuple max_tuple) ;
			("term_type_nth", term_type_nth max_tuple) ;
			("spec_compute", spec_compute self sem) ;
			("get_field_value", get_field_value fields) ;
			("record_compute", record_compute sem) ;
			("field_reset", field_reset sem) ;
			("extend_env_tuple", extend_env_tuple max_tuple) ;
			("eval_term_tuple", eval_term_tuple max_tuple) ;
			("eval_term_nth", eval_term_nth max_tuple) ;
			("print_term_tuple", print_term_tuple max_tuple) ;
			("print_term_nth", print_term_nth max_tuple) ;
			("print_pattern_tuple", print_pattern_tuple max_tuple) ;
			("untuple_func", untuple_func max_tuple) ;
			("tuple_cont", tuple_cont max_tuple) ;
			("nth_cont", nth_cont max_tuple) ;
			("tuple_string_of_cont", tuple_string_of_cont max_tuple) ;
			("nth_string_of_cont", nth_string_of_cont max_tuple) ;
			("term_step_tuple", term_step_tuple max_tuple) ;
			("term_step_nth", term_step_nth max_tuple) ;
			("cont_step_tuple", cont_step_tuple max_tuple) ;
			("cont_step_nth", cont_step_nth max_tuple) ;
			("breakpoint_options", breakpoint_options sem) ;
			("apply_spec", apply_spec self sem) ;
			("apply_spec_sig", apply_spec_sig sem) ;
			("spec_terms", spec_terms sem) ;
		]
	in
	(* open template file and return specialized result *)
	let site_path =
		begin match Mysites.Sites.template with
		| [ a ] -> a
		| _ -> assert false
		end
	in
	let template_filename = site_path ^ "/debugtemplate.ml" in
	let ic = open_in template_filename in
	let contents = getlines ic in
	let contents = search_and_replace map contents in
	let contents = replace_special_comment contents in
	let contents = search_and_refute "c" (constructors = []) contents in
	let contents = search_and_refute "1c" (List.length constructors <= 1) contents in
	let contents = search_and_refute "f" (fields = []) contents in
	let contents = search_and_refute "1f" (List.length fields <= 1) contents in
	let contents = search_and_refute "u" (all_unspec_types = []) contents in
	let contents = search_and_refute "s" (all_spec_types = []) contents in
	let contents = search_and_refute "ut" (all_unspec_terms = []) contents in
	let contents = search_and_refute "st" (all_spec_terms = []) contents in

	let contents = search_and_anonymize "f" (fields = []) contents in
	let contents = search_and_anonymize "c" (constructors = []) contents in
	let contents = search_and_anonymize "st" (all_spec_terms = []) contents in
	let contents = search_and_anonymize "ut" (all_unspec_terms = []) contents in

	let contents = search_and_remove "f" (fields = []) contents in
	let contents = search_and_remove "f'" (fields <> []) contents in
	let contents = search_and_remove "c" (constructors = []) contents in
	contents
