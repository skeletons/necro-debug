(** This file is a template, it is not designed to be used as is, but as a part of Necro Debug (https://gitlab.inria.fr/skeletons/necro-debug) *)

module SMap = Map.Make(String)

module type TYPES = sig{# unspec_types #}
end

module type SPEC = sig
	include TYPES
{# spec_types #}{# packing #}{# unspec_terms #}{# unspec_print_module_type #}
end

module Spec(T:TYPES) = struct
	include T{# spec_types #}{# packing #}{# default_print #}
end

module type PRINT = sig
	include SPEC{# print_module_type #}
end

module Print(S:SPEC) = struct
	include S{# spec_printers #}
end










module type DEBUGGER = functor (P : PRINT) ->
	sig

		(* Main interface for the machine *)

		type _ amstate

		type action =
			| Next | Previous
			| FastForward | Start
			| Step | Drop
			| GoTo of Int.t
			| PickBranch of Int.t
			| Breakpoint of String.t

		val perform: action -> 'a amstate -> ('a amstate, String.t) Result.t

		val compute : 'a amstate -> 'a Option.t

		val spec_terms: String.t List.t


		(* Initiate the machine *)

		{# apply_spec_sig #}



		type state_kind = | Term | Skeleton | Value | Fail | Apply
		type printing =
			{ environment: String.t
			; continuation: String.t
			; current: String.t
			; backtrack: String.t
			; counter: Int.t
			; kind: state_kind
			}
		val print_state: _ amstate -> printing

	end



module MakeDebugger: DEBUGGER = functor (P:PRINT) -> struct
	module AbstractMachine = struct
		include P
		type _ witness =
		| Unit: Unit.t witness
		{# tuple_witness #}
		| Arrow: 'a witness * 'b witness -> ('a -> 'b) witness
		{# type_witness #}

		type (_,_) constructor =
		{# constr #}

		type (_,_) field =
		{# field #}

		let constr_type: type a b. (a, b) constructor -> b witness =
			begin function
			{# constr_type #}
			end

		let field_type: type a b. (a, b) field -> b witness =
			begin function
			{# field_type #}
			end

		{# untuple_type #}let constr_compute : type a b. (a,b) constructor -> a -> b =
			fun c a ->
			begin match c with
			{# constr_compute #}
			end

		let field_compute : type a b. (a,b) field -> a -> b =
			fun f {{f_:a}} ->
			begin match f with
			{# field_compute #}
			end

		let remove_constr : type a b. (a,b) constructor -> b -> a Option.t =
			fun c a ->
			begin match c, a with
			{# remove_constr #}
			| _, _ ->{{1c: None}}
			end

		type _ spec =
			{# spec #}

		type _ unspec =
			{# unspec #}

		let spec_type: type a. a spec -> a witness =
			fun s ->
			begin match s with
			{# spec_type #}
			end

		let unspec_type: type a. a unspec -> a witness =
			fun u ->
			begin match u with
			{# unspec_type #}
			end

		let unspec_compute: type a. a unspec -> a =
			fun u ->
			begin match u with
			{# unspec_compute #}
			end

		exception MatchEmpty
		let rec string_of_wit: type a. a witness -> String.t =
			begin function
			| Arrow (i, o) ->
					Format.sprintf "(%s → %s)" (string_of_wit i) (string_of_wit o)
			| Unit -> "()"
			{# tuple_string_of_wit #}
			{# type_string_of_wit #}
			end

		let rec print_val: type a. a witness -> a -> String.t =
			fun w v ->
			begin match w with
			| Unit -> "()"
			| Arrow _ -> "(fun)"
			{# print_val_tuple #}
			{# print_val_type #}
			end

		let print_constr: type a b. (a, b) constructor -> String.t =
			fun c ->
			begin match c with
			{# print_constr #}
			end

		let print_field: type a b. (a, b) field -> String.t =
			fun f ->
			begin match f with
			{# print_field #}
			end

		let print_spec: type a. a spec -> String.t =
			fun s ->
			begin match s with
			{# print_spec #}
			end

		let print_unspec: type a. a unspec -> String.t =
			fun s ->
			begin match s with
			{# print_unspec #}
			end

		module type TYPET = sig type _ t end
		module Caster(T:TYPET) = struct
			type (_, _) eq = Refl: ('a, 'a) eq
			let rec geteq: type a b. a witness -> b witness -> (a, b) eq =
				fun wa wb ->
				begin match wa, wb with
				| Unit, Unit -> Refl
				| Arrow (ia, oa), Arrow (ib, ob) ->
						let eqi = geteq ia ib in
						let eqo = geteq oa ob in
						begin match eqi, eqo with Refl, Refl -> Refl end
				{# get_eq_tuple #}
				{# get_eq_type #}
				| _, _ -> Format.kasprintf failwith "cannot match type %s with type %s" (string_of_wit wa)
				(string_of_wit wb)
				end
			let cast: type a b. a witness -> b witness -> a T.t -> b T.t
			= fun wa wb v ->
				let eq: (a, b) eq = geteq wa wb in
				match eq with | Refl -> v
		end


		type 'a unspec_app =
		| PUUnspec : 'a unspec -> 'a unspec_app
		| PUApp  : ('a -> 'b) unspec_app * 'a witness * 'a typed_value -> 'b
		unspec_app

		and 'a spec_app =
		| PSSpec : 'a spec -> 'a spec_app
		| PSApp  : ('a -> 'b) spec_app * 'a witness * 'a typed_value -> 'b spec_app

		and 'a typed_value ={# typed_value_tuple #}
		| Value: 'a -> 'a typed_value
		| Term: 'a term -> 'a typed_value
		| Clos: env * 'a pattern * 'b skeleton -> ('a -> 'b) typed_value
		| VUnspec: 'a unspec_app -> 'a typed_value
		| VSpec: 'a spec_app -> 'a typed_value

		and value =
			Pack: 'a typed_value * 'a witness -> value

		and 'a typed_value_record =
		| VNil: 'a typed_value_record
		| VCons: ('a, 'b) field * 'b typed_value * 'a typed_value_record -> 'a typed_value_record

		and env = value SMap.t

		and 'a pattern_record =
		| PNil: 'a pattern_record
		| PCons: ('a, 'b) field * 'b pattern * 'a pattern_record -> 'a pattern_record

		and _ pattern =
		| PWild : 'a witness -> 'a pattern
		| Ptt: Unit.t pattern
		| PVar : String.t * 'a witness -> 'a pattern
		| PConstr: ('a, 'b) constructor * 'a pattern -> 'b pattern
		| PRecord: 'a witness * 'a pattern_record -> 'a pattern
		| POr : 'a pattern * 'a pattern -> 'a pattern
		{# pattern_tuple #}

		and 'a term_record =
		| TNil: 'a term_record
		| TCons: ('a, 'b) field * 'b term * 'a term_record -> 'a term_record

		and _ term =
		| Var: String.t * 'a witness -> 'a term
		| Spec: 'a spec -> 'a term
		| Unspec: 'a unspec -> 'a term
		| Constr : ('a, 'b) constructor * 'a term -> 'b term
		| Tt : Unit.t term
		{# term_tuple #}
		| Func : 'a pattern * 'b skeleton -> ('a -> 'b) term
		| Field: 'a term * ('a, 'b) field -> 'b term
		{# term_nth #}
		| RecMake: 'a witness * 'a term_record -> 'a term
		| RecSet: 'a term * 'a term_record -> 'a term

		and _ skeleton =
		| Return: 'a term -> 'a skeleton
		| Branching: 'a witness * 'a skeleton List.t -> 'a skeleton
		| Match: 'a skeleton * ('a pattern * 'b skeleton) List.t -> 'b skeleton
		| LetIn: 'a pattern * 'a skeleton * 'b skeleton -> 'b skeleton
		| Apply: ('a -> 'b) term * 'a term -> 'b skeleton
		(* exists: LATER *)

		let rec partial_unspec_name: type a. a unspec_app -> String.t = fun u ->
			begin match u with
			| PUApp (x, _, _) -> partial_unspec_name x
			| PUUnspec u -> print_unspec u
			end

		let rec partial_spec_name: type a. a spec_app -> String.t = fun u ->
			begin match u with
			| PSApp (x, _, _) -> partial_spec_name x
			| PSSpec u -> print_spec u
			end

		let rec pattern_type: type a. a pattern -> a witness =
			fun p ->
			begin match p with
			| PWild ty -> ty
			| Ptt -> Unit
			| PVar (_, ty) -> ty
			| POr (p, _) -> pattern_type p
			| PConstr (c, _) ->{{c: constr_type c}}
			| PRecord (ty, _) -> ty
			{# pattern_type_tuple #}
			end

		let rec skel_type: type a. a skeleton -> a witness =
			fun s ->
			begin match s with
			| Return t -> term_type t
			| Branching (ty, _) -> ty
			| Match (_, []) -> failwith "empty matchings are not allowed"
			| Match (_, (_, s) :: _) -> skel_type s
			| LetIn (_, _, s) -> skel_type s
			| Apply (f, _) ->
					begin match term_type f with
					| Arrow (_, b) -> b
					| _ ->{{u: failwith "Can only apply arrow types"}}
					end
			end

		and term_type: type a. a term -> a witness =
			fun t ->
			begin match t with
			| Var (_, ty) -> ty
			| Constr (c, _) ->{{c: constr_type c}}
			| Tt -> Unit
			{# term_type_tuple #}
			| Func (p, s) -> Arrow (pattern_type p, skel_type s)
			| Spec {{st_:s}} ->{{st: spec_type s}}
			| Unspec {{ut_:u}} ->{{ut: unspec_type u}}
			| Field {{f_:(_, f)}} ->{{f: field_type f}}
			{# term_type_nth #}
			| RecMake (ty, _) -> ty
			| RecSet (t, _) -> term_type t
			end

		and partial_unspec_type: type a. a unspec_app -> a witness =
			fun u ->
			begin match u with
			| PUUnspec t ->{{ut: unspec_type t}}
			| PUApp (f, _, _) ->
					begin match partial_unspec_type f with
					| Arrow (_, b) -> b
					| _ -> failwith "Can only apply arrow types"
					end
			end

		and partial_spec_type: type a. a spec_app -> a witness =
			fun u ->
			begin match u with
			| PSSpec {{st_:t}} ->{{st: spec_type t}}
			| PSApp (f, _, _) ->
					begin match partial_spec_type f with
					| Arrow (_, b) -> b
					| _ -> failwith "Can only apply arrow types"
					end
			end
		let spec_compute: type a. a spec -> a term =
			fun s ->
			begin match s with
			{# spec_compute #}
			end




		let cast_term: type a b. a witness -> b witness -> a term -> b term =
			fun a b v -> let module C = Caster(struct type 'a t = 'a term end) in C.cast a b v

		let cast_val: type a b. a witness -> b witness -> a typed_value -> b typed_value =
			fun a b v -> let module C = Caster(struct type 'a t = 'a typed_value end) in C.cast a b v

		let {{fr:rec }}append: type a b.
				(a, b) field -> b typed_value ->
				a typed_value_record ->
				a typed_value_record =
			fun f v fvl ->
			begin match fvl with
			| VNil -> VCons (f, v, VNil)
			| VCons {{f_:(f', v', fvq)}} ->{{f: VCons (f', v', append f v fvq)}}
			end

		let {{fr:rec }}get_field_value:
				type a b. (a, b) field -> a typed_value_record -> b typed_value =
			fun f fvl ->
			begin match f, fvl with
			| _, VNil -> failwith ("unbound field " ^ print_field f)
			{# get_field_value #}
			| _, VCons {{f_:(_, _, fql)}} ->{{1f: get_field_value f fql}}
			end

		{{fr:and }}{{f'r:let rec }}record_compute:
				type a. a witness -> a typed_value_record -> a typed_value =
			fun w {{f_:fvl}} ->
				begin match w with
				{# record_compute #}
				| _ -> failwith ("Type " ^ (string_of_wit w) ^ " is not a record type")
				end

		(* field_reset _ v f w is v ← (f=w) *)
		and field_reset: type a b.
				a typed_value -> (a, b) field -> b typed_value -> a typed_value =
			fun {{f_:v}} f {{f_:w}} ->
			begin match f with
			{# field_reset #}
			end


		(* record_reset _ v fvl is v ← (fvl) *)
		and record_reset: type a.
				a witness -> a typed_value -> a typed_value_record -> a typed_value =
			fun {{f_:ty}} v fvl ->
				begin match fvl with
				| VNil -> v
				| VCons {{f_:(f, w, fvq)}} ->{{f: record_reset ty (field_reset v f w) fvq}}
				end


		and extend_env: type a. env -> a pattern -> a typed_value -> env Option.t = fun e p v ->
			begin match p with
			| PWild _ -> Some e
			| PVar (x, ty) -> Some (SMap.add x (Pack (v, ty)) e)
			| PConstr (c, p) ->{{c:
				begin match remove_constr c (from_typed_value v) with
				| Some w -> extend_env e p (Value w)
				| None -> None
				end}}
			| Ptt -> Some e
			| POr (p1, p2) ->
					begin match extend_env e p1 v with
					| Some e' -> Some e'
					| None -> extend_env e p2 v
					end
			{# extend_env_tuple #}
			| PRecord (_, pl) -> extend_env_rec e pl v
			end

		and extend_env_rec:
				type a. env -> a pattern_record -> a typed_value -> env Option.t =
			fun e pl {{f_:v}} ->
			begin match pl with
			| PNil -> Some e
			| PCons {{f_:(f, p, pq)}} ->{{f:
					let w = field_compute f (from_typed_value v) in
					Option.bind
						(extend_env e p (Value w))
						(fun e' -> extend_env_rec e' pq v)}}
			end

		and from_typed_value: type a. a typed_value -> a = fun x ->
			begin match x with{# from_typed_value_tuple #}
			| Value x -> x
			| Clos (e, p, s) ->
					fun x ->
						begin match extend_env e p (Value x) with
						| None -> failwith "pattern matching failed"
						| Some e' -> from_typed_value (eval_skel e' s)
						end
			| Term t -> from_typed_value (eval_term SMap.empty t)
			| VUnspec (PUUnspec {{ut_:u}}) ->{{ut: unspec_compute u}}
			| VUnspec (PUApp (f, _, x)) -> from_typed_value (VUnspec f) @@ from_typed_value x
			| VSpec (PSSpec {{st_:s}}) ->{{st: from_typed_value @@ Term (spec_compute s)}}
			| VSpec (PSApp (f, _, x)) ->
					from_typed_value (VSpec f) @@ from_typed_value x
			end

		and partial_unspec_compute: type a. a unspec_app -> a =
			fun u ->
			begin match u with
			| PUUnspec u ->{{ut: unspec_compute u}}
			| PUApp (f, _, x) -> partial_unspec_compute f @@ from_typed_value x
			end

		{# untuple_func #}

		and eval_term : type a. env -> a term -> a typed_value = fun e t ->
			begin match t with
			| Constr (c, x) ->{{c:
				Value (constr_compute c @@ from_typed_value @@ eval_term e x)}}
			| Tt -> Value ()
			| RecMake (ty, ftl) ->
					let fvl = eval_term_record e ftl in
					record_compute ty fvl
			| RecSet (t, ftl) ->
					let v = eval_term e t in
					let fvl = eval_term_record e ftl in
					record_reset (term_type t) v fvl
			| Field {{f_:(t, f)}} ->{{f:
				Value (field_compute f @@ from_typed_value @@ eval_term e t)}}
			{# eval_term_tuple #}
			{# eval_term_nth #}
			| Var (x, typ) ->
				begin match SMap.find_opt x e with
				| None -> failwith (x ^ " was not found in the environment.\n" ^
					"Current environment:\n" ^ print_env e)
				| Some (Pack (v, tyv)) -> cast_val tyv typ v
				end
			| Func (p, sk) -> Clos (e, p, sk)
			| Spec {{st_:s}} ->{{st: Term (spec_compute s)}}
			| Unspec s ->{{ut: Value (unspec_compute s)}}
			end

		and eval_term_record: type a. env -> a term_record -> a typed_value_record =
			fun {{f_:e}} ftl ->
			begin match ftl with
			| TNil -> VNil
			| TCons {{f_:(f, t, ftq)}} ->{{f:
					VCons (f, eval_term e t, eval_term_record e ftq)}}
			end

		and print_typed_value: type a. a witness -> a typed_value -> String.t =
			fun w v ->
			begin match v with
			{# print_typed_value_tuple #}
			| Value v -> print_val w v
			| Term t -> print_term t
			| Clos (_, p, s) ->
				Format.sprintf "λ%s·%s" (print_pattern p) (print_skeleton s)
			| VUnspec (PUUnspec u) ->{{ut: print_unspec u}}
			| VUnspec (PUApp (f, tyb, x)) ->
				Format.sprintf "%s %s"
				(print_typed_value (partial_unspec_type f) (VUnspec f))
				(print_typed_value tyb x)
				| VSpec (PSSpec {{st_:u}}) ->{{st: print_spec u}}
			| VSpec (PSApp (f, tyb, x)) ->
				Format.sprintf "%s %s"
				(print_typed_value (partial_spec_type f) (VSpec f))
				(print_typed_value tyb x)
			end

		and print_typed_value_record: type a. a typed_value_record -> String.t = fun fvl ->
			begin match fvl with
			| VNil -> ""
			| VCons {{f_:(f, v, VNil)}} ->{{f:
					Format.sprintf "%s = %s"
					(print_field f) (print_typed_value (field_type f) v)
			| VCons (f, v, ftq) ->
					Format.sprintf "%s = %s, %s"
					(print_field f) (print_typed_value (field_type f) v)
					(print_typed_value_record ftq)}}
			end

		and print_skeleton: type a. ?indent:Int.t -> a skeleton -> String.t =
			fun ?(indent=0) s ->
			let open Format in
			let nl = "\n" ^ String.make indent ' ' in
			begin match s with
			| Branching (_, []) -> "branch end"
			| Branching (_, sl) ->
					let sep = nl ^ "or" ^ nl ^ "  " in
					sprintf "branch%s  %s%send"
					nl (String.concat sep (List.map (print_skeleton ~indent:(indent+2)) sl)) nl
			| Match (s, psl) ->
					sprintf "match %s with%s%send" (print_skeleton ~indent s)
					nl (List.map (fun (p,s) ->
						sprintf "| %s -> %s%s" (print_pattern p) (print_skeleton
						~indent:(indent+2) s) nl) psl |> String.concat "")
			| LetIn (p, s1, s2) ->
					sprintf "let %s = %s in%s%s" (print_pattern p)
					(print_skeleton ~indent s1) nl (print_skeleton ~indent s2)
			| Apply (f, x) -> sprintf "%s %s"
					(print_term ~indent ~protect:true f)
					(print_term ~indent ~protect:true x)
			| Return t -> print_term ~indent t
			end

		and print_term: type a. ?indent:Int.t -> ?protect:Bool.t -> a term -> String.t =
			fun ?(indent=0) ?(protect=false) t ->
			let open Format in
			let nl =
				"\n" ^ String.concat "" (List.init indent (Fun.const " ")) in
			begin match t with
			| Var (x, _) -> x
			| Spec s ->{{st: print_spec s}}
			| Unspec u ->{{ut: print_unspec u}}
			| Constr (c, t) ->{{c:
				if protect then
					sprintf "(%s %s)" (print_constr c) (print_term ~indent ~protect t)
				else
					sprintf "%s %s" (print_constr c) (print_term ~indent ~protect:true t)}}
			| Tt -> "()"
			{# print_term_tuple #}
			| Func (p, s) when protect ->
					sprintf "(λ %s →%s  %s)"
					(print_pattern p) nl (print_skeleton ~indent:(indent+2) s)
			| Func (p, s) ->
					sprintf "λ %s →%s  %s"
					(print_pattern p) nl (print_skeleton ~indent:(indent+2) s)
			| Field {{f_:(t, f)}} ->{{f:
					Format.sprintf "%s.%s" (print_term ~indent ~protect t) (print_field f)}}
			{# print_term_nth #}
			| RecMake (_, ftl) ->
					Format.sprintf "(%s)" (print_term_record ~indent ftl)
			| RecSet (t, ftl) ->
					Format.sprintf "%s ← (%s)" (print_term ~indent t) (print_term_record
					~indent ftl)
			end
		and print_term_record: type a. ?indent:Int.t -> a term_record -> String.t =
			fun ?(indent=0) ftl ->{{f'r: ignore indent;}}
			begin match ftl with
			| TNil -> ""
			| TCons {{f_:(f, t, TNil)}} ->{{f:
					Format.sprintf "%s = %s" (print_field f) (print_term ~indent t)
			| TCons (f, t, ftq) ->
					Format.sprintf "%s = %s, %s" (print_field f) (print_term ~indent t)
					(print_term_record ftq)}}
			end

		and print_pattern: type a. ?protect:Bool.t -> a pattern -> String.t = fun
			?(protect=false) p ->
			let open Format in
			begin match p with
			| PWild _ -> "_"
			| PVar (x, _) -> x
			| PConstr (c, p) ->{{c:
					sprintf "%s %s" (print_constr c) (print_pattern ~protect:true p)
					|> if protect then sprintf "(%s)" else Fun.id}}
			{# print_pattern_tuple #}
			| POr (p1, p2) when protect ->
					sprintf "(%s | %s)" (print_pattern p1) (print_pattern p2)
			| POr (p1, p2) ->
					sprintf "%s | %s" (print_pattern p1) (print_pattern p2)
			| Ptt -> "()"
			| PRecord (_, fpl) ->
					sprintf "(%s)" (print_pattern_record fpl)
			end
		and print_pattern_record: type a. a pattern_record -> String.t = fun fpl ->
			{{fr:let open Format in
			}}begin match fpl with
			| PNil -> ""
			| PCons {{f_:(f, p, PNil)}} ->{{f:
					sprintf "%s = %s" (print_field f) (print_pattern p)
			| PCons (f, p, ftq) ->
					sprintf "%s = %s, %s" (print_field f) (print_pattern p)
					(print_pattern_record ftq)}}
			end

		and print_env e =
			let rec aux l =
				begin match l with
				| [] -> ""
				| (name, (Pack (v, w))) :: q ->
						Format.sprintf "- %s ← %s"
						name (print_typed_value w v) ^ "\n" ^
						aux q
				end
			in aux (SMap.bindings e)


		and eval_skel : type a. env -> a skeleton -> a typed_value = fun e sk ->
			begin match sk with
			| Return t -> eval_term e t
			| Branching (_, sl) ->
					let s1 = List.hd sl in
					eval_skel e s1
			| Match (_, []) -> raise MatchEmpty
			| Match (t, (p, s) :: psl) ->
					let v = eval_skel e t in
					begin match extend_env e p v with
					| None -> eval_skel e (Match (t, psl))
					| Some e' -> eval_skel e' s
					end
			| LetIn (p, b, s) ->
					let v = eval_skel e b in
					begin match extend_env e p v with
					| None -> failwith "pattern matching not successful"
					| Some e' -> eval_skel e' s
					end
			| Apply (f, x) ->
					let f = eval_term e f in
					let x = eval_term e x in
					apply f x
			end

			and apply: type a b. (a -> b) typed_value -> a typed_value -> b typed_value =
				fun f x -> Value ((from_typed_value f) (from_typed_value x))

		type ('a, 'b) cont = (* continuation with a hole of type 'a, which represents
		a value of type 'b *)
			| MarkStep: ('a, 'b) cont -> ('a, 'b) cont
			| Kid: ('a, 'a) cont (* identity continuation *)
			| Kconstr: ('a, 'b) constructor * ('b, 'c) cont -> ('a, 'c) cont
			{# tuple_cont #}
			{# nth_cont #}
			| Kmatch: env * ('a pattern * 'b skeleton) List.t * ('b, 'c) cont -> ('a, 'c) cont
			| Klet: env * 'a pattern * 'b skeleton * ('b, 'c) cont -> ('a, 'c) cont
			| Kapply_1: env * 'a term * ('b, 'c) cont -> ('a -> 'b, 'c) cont
			| Kapply_2: ('a -> 'b) typed_value * ('a -> 'b) witness * ('b, 'c) cont -> ('a, 'c) cont
			| Kapply_3: 'a witness * 'a typed_value * ('b, 'c) cont -> ('a -> 'b, 'c) cont
			| Kfield: ('a, 'b) field * ('b, 'c) cont -> ('a, 'c) cont
			| Krecmake: 'b witness * env * 'b typed_value_record * ('b, 'a) field * 'b term_record * ('b, 'c) cont -> ('a, 'c) cont
			(* Kset (e, ftl, k)  is   k [ □ ← ftl(in env e) ]   *)
			| Kset: env * 'a term_record * ('a, 'b) cont -> ('a, 'b) cont
			(* Kset2 (v, _, f, k)  is   k [ v ← (f ← □) ]   *)
			| Kset2: 'a typed_value * 'a witness * ('a, 'b) field * ('a, 'c) cont -> ('b, 'c) cont


		let rec string_of_cont: type a b. (a, b) cont -> String.t =
			fun k ->
			let open Format in
			begin match k with
			| MarkStep k -> string_of_cont k
			| Kid -> ""
			| Kconstr (c, k) ->{{c: sprintf
				"- %s □\n%s" (print_constr c) (string_of_cont k)}}
			{# tuple_string_of_cont #}
			| Kmatch (_, psl, k) -> sprintf
				"- match □ with\n%s\nend%s"
				(List.map (fun (p, s) ->
					sprintf "| %s → %s" (print_pattern p) (print_skeleton s)) psl
				|> String.concat "\n") (string_of_cont k)
			| Klet (_, p, sk, k) -> sprintf
				"- let %s = □ in\n%s\n%s"
				(print_pattern p) (print_skeleton sk) (string_of_cont k)
			| Kapply_1 (_, x, k) -> sprintf
				"- □ %s\n%s" (print_term x) (string_of_cont k)
			| Kapply_2 (f, ty, k) -> sprintf
				"- %s □\n%s" (print_typed_value ty f) (string_of_cont k)
			| Kapply_3 (ty, v, k) -> sprintf
				"- □ %s\n%s" (print_typed_value ty v) (string_of_cont k)
			| Kfield {{f_:(f, k)}} ->{{f: sprintf
				"- □.%s\n%s" (print_field f)
				(string_of_cont k)}}
			{# nth_string_of_cont #}
			| Krecmake {{f_:(_, _, VNil, f, TNil, k)}} ->{{f: sprintf
				"- (%s = □)\n%s"
				(print_field f)
				(string_of_cont k)
			| Krecmake (_, _, fvl, f, TNil, k) -> sprintf
				"- (%s, %s = □)\n%s"
				(print_typed_value_record fvl)
				(print_field f)
				(string_of_cont k)
			| Krecmake (_, _, VNil, f, ftl, k) -> sprintf
				"- (%s = □, %s)\n%s"
				(print_field f)
				(print_term_record ftl)
				(string_of_cont k)
			| Krecmake (_, _, fvl, f, ftl, k) -> sprintf
				"- (%s, %s = □, %s)\n%s"
				(print_typed_value_record fvl)
				(print_field f)
				(print_term_record ftl)
				(string_of_cont k)}}
			| Kset (_, ftl, k) -> sprintf
				"□ ← (%s)\n%s"
				(print_term_record ftl)
				(string_of_cont k)
			| Kset2 {{f_:(v, ty, f, k)}} ->{{f: sprintf
				"%s ← (%s = □)\n%s"
				(print_typed_value ty v) (print_field f) (string_of_cont k)}}
			end

		type 'a configuration =
			| Skel: env * 'a skeleton * ('a, 'b) cont * 'b fk -> 'b configuration
			| Term: env * 'a term * ('a, 'b) cont * 'b fk -> 'b configuration
			| Ret: 'a typed_value * 'a witness * ('a, 'b) cont * 'b fk -> 'b configuration
			| Fail: 'a fk -> 'a configuration
			| Apply: ('a -> 'b) typed_value * 'a typed_value * 'a witness * 'b witness * ('b, 'c) cont * 'c fk -> 'c configuration
			| CUnspec: 'a unspec_app * ('a, 'b) cont * 'b fk -> 'b configuration
			| CSpec: 'a spec_app * ('a, 'b) cont * 'b fk -> 'b configuration
		and 'a fk = 'a configuration Option.t

		let drop: type a. a configuration -> a configuration =
			fun cfg ->
				begin match cfg with
				| Skel (_, _, _, f)
				| Term (_, _, _, f)
				| Ret (_, _, _, f)
				| Fail f
				| Apply (_, _, _, _, _, f)
				| CUnspec (_, _, f)
				| CSpec (_, _, f) ->
						Fail f
				end


		let rec step: type a. a configuration -> a configuration =
			fun cfg ->
			begin match cfg with
			| Fail None -> (* computation failed *)
					Fail None
			| Fail (Some cfg) -> (* backtrack *)
					cfg
			| Term (e, t, k, fk) -> term_step e t k fk
			| Skel (e, sk, k, fk) -> skel_step e sk k fk
			| Ret (v, ty, k, fk) -> cont_step v ty k fk
			| Apply (f, x, tya, tyb, k, fk) -> apply_step f x tya tyb k fk
			| CUnspec (u, k, fk) -> unspec_step u k fk
			| CSpec (u, k, fk) -> spec_step u k fk
			end

		and term_step:
				type a b. env -> a term -> (a, b) cont -> b fk -> b configuration =
			fun e t k fk ->
			begin match t with
			| Tt -> Ret (Value (), Unit, k, fk)
			| Var (x, tya) ->
					begin match SMap.find_opt x e with
					| None -> failwith ("Unbound variable " ^ x)
					| Some (Pack (Term t, tyv)) ->
							Term (e, cast_term tyv tya t, k, fk)
					| Some (Pack (v, tyv)) -> Ret (cast_val tyv tya v, tya, k, fk)
					end
			| Spec {{st_:s}} ->{{st: CSpec (PSSpec s, k, fk)}}
			| Unspec s ->{{ut: CUnspec (PUUnspec s, k, fk)}}
			| Constr (c, t) ->{{c: Term (e, t, Kconstr (c, k), fk)}}
			| Func (p, s) ->
					Ret (Clos (e, p, s), Arrow (pattern_type p, skel_type s), k, fk)
			| Field {{f_:(t, f)}} ->{{f:
					Term (e, t, Kfield (f, k), fk)}}
			{# term_step_tuple #}
			{# term_step_nth #}
			| RecMake (_, TNil) -> Fail fk
			| RecMake {{f_:(ty, TCons (f, t, ftl))}} ->{{f:
					Term (e, t, Krecmake (ty, e, VNil, f, ftl, k), fk)}}
			| RecSet (t, set) -> Term (e, t, Kset (e, set, k), fk)
			end

		and skel_step:
				type a b. env -> a skeleton -> (a, b) cont -> b fk -> b configuration =
			fun e sk k fk ->
			begin match sk with
			| Return t -> Term (e, t, k, fk)
			| Branching (_, []) -> Fail fk
			| Branching (_, [s]) -> Skel (e, s, k, fk)
			| Branching (ty, s1::sq) ->
					(* if branch 1 fails, take other branches before going to fk *)
					let fk = Some (Skel (e, Branching (ty, sq), k, fk)) in
					Skel (e, s1, k, fk)
			| Match (t, psl) -> Skel (e, t, Kmatch (e, psl, k), fk)
			| LetIn (p, s1, s2) -> Skel (e, s1, Klet (e, p, s2, k), fk)
			| Apply (f, x) -> Term (e, f, Kapply_1 (e, x, k), fk)
			end

		and cont_step: type a b.
				a typed_value -> a witness -> (a, b) cont -> b fk -> b configuration =
			fun v ty k fk ->
			begin match k with
			| MarkStep k -> cont_step v ty k fk
			| Kid -> Ret (v, ty, Kid, fk) (* Computation over *)
			| Kconstr (c, k) ->{{c:
				Ret (Value (constr_compute c @@ from_typed_value v), constr_type c, k, fk)}}
			{# cont_step_tuple #}
			| Kmatch (_, [], _) -> Fail fk
			| Kmatch (e, (p, sk)::psl, k) ->
				begin match extend_env e p v with
				| None -> Ret (v, ty, Kmatch (e, psl, k), fk)
				| Some e' -> Skel (e', sk, k, fk)
				end
			| Klet (e, p, s, k) ->
				begin match extend_env e p v with
				| None -> Fail fk
				| Some e' -> Skel (e', s, k, fk)
				end
			| Kapply_1 (e, x, k) ->
				Term (e, x, Kapply_2 (v, ty, k), fk)
			| Kapply_2 (f, ty, k) ->
				begin match ty with
				| Arrow (tya, tyb) -> Apply (f, v, tya, tyb, k, fk)
				| _ ->{{u: failwith "Can only apply arrow types"}}
				end
			| Kapply_3 (_tyv, w, k) ->
				begin match ty with
				| Arrow (tya, tyb) -> Apply (v, w, tya, tyb, k, fk)
				| _ -> failwith "Can only apply arrow types"
				end
			| Kfield {{f_:(f, k)}} ->{{f:
				Ret (Value (field_compute f (from_typed_value v)), field_type f, k, fk)}}
			{# cont_step_nth #}
			| Krecmake {{f_:(ty, e, fvl, f, TCons (f', t, ftl), k)}} ->{{f:
				let new_fvl = append f v fvl in
				Term (e, t, Krecmake (ty, e, new_fvl, f', ftl, k), fk)
			| Krecmake (ty, _, fvl, f, TNil, k) ->
					Ret (record_compute ty (VCons (f, v, fvl)), ty, k, fk)}}
			| Kset (_, TNil, k) -> Ret (v, ty, k, fk)
			| Kset {{f_:(e, TCons (f, t, ftl), k)}} ->{{f:
					Term (e, t, Kset2 (v, ty, f, Kset (e, ftl, k)), fk)}}
			| Kset2 {{f_:(w, tyw, f, k)}} ->{{f:
					Ret (field_reset w f v, tyw, k, fk)}}
			end

		and apply_step:
				type a b c. (a -> b) typed_value -> a typed_value -> a witness
				-> b witness -> (b, c) cont -> c fk -> c configuration =
			fun f x tya tyb k fk ->
			begin match f with
			| Clos (e, p, s) ->
					begin match extend_env e p x with
					| None -> Fail fk
					| Some e' -> Skel (e', s, k, fk)
					end
			| Value f -> Ret (Value (f @@ from_typed_value x), tyb, k, fk)
			| Term _ -> failwith "Unexpected behaviour"
			| VUnspec u -> CUnspec (PUApp (u, tya, x), k, fk)
			| VSpec u -> CSpec (PSApp (u, tya, x), k, fk)
			end

		and unspec_step:
				type a b. a unspec_app -> (a, b) cont -> b fk -> b configuration =
			fun u k fk ->
			let ty = partial_unspec_type u in
			begin match ty with
			| Arrow _ -> step @@ Ret (VUnspec u, ty, k, fk)
			| _ -> Ret (Value (partial_unspec_compute u), ty, k, fk)
			end

		and spec_step:
				type a b. a spec_app -> (a, b) cont -> b fk -> b configuration =
			fun s k fk ->
			let ty = partial_spec_type s in
			begin match ty with
			| Arrow _ -> step @@ Ret (VSpec s, ty, k, fk)
			| _ -> partial_spec_compute s k fk
			end

		and partial_spec_compute:
				type a b. a spec_app -> (a, b) cont -> b fk -> b configuration =
			fun s k fk ->
			begin match s with
			| PSSpec {{st_:s}} ->{{st: Term (SMap.empty, spec_compute s, k, fk)}}
			| PSApp (f, tyx, x) ->
					let newk = (* continuation for f : evaluate then apply to x *)
						Kapply_3 (tyx, x, k) in
					partial_spec_compute f newk fk
			end
	end

	open AbstractMachine

	type action =
		| Next | Previous
		| FastForward | Start
		| Step | Drop
		| GoTo of Int.t
		| PickBranch of Int.t
		| Breakpoint of String.t

	type 'a amstate = {
		cpt: Int.t ;
		beg: 'a configuration ;
		cur: 'a configuration ;
		prev: 'a configuration List.t ;
	}

	let over: type a. a configuration -> Bool.t =
		begin function
		| Fail None -> true
		| Ret (_, _, Kid, _) -> true
		| _ -> false
		end

	let (let*) = Result.bind

	let rec perform action log =
		begin match action with
		| Next ->
				let cpt = log.cpt + 1 in
				let prev = log.cur :: log.prev in
				let cur = step log.cur in
				Ok {log with cpt;cur;prev}
		| Previous ->
				begin match log.prev with
				| [] ->
						Error "There is no previous step."
				| a :: q ->
						let cur = a in
						let prev = q in
						let cpt = log.cpt - 1 in
						Ok {log with cur;prev;cpt}
				end
		| GoTo i ->
				let rec iter x n log =
					if n = 0 then
						Ok log
					else
						let* log = perform x log in
						iter x (n - 1) log
				in
				if i < 0 then
					Error "Cannot go to negative step"
				else if i < log.cpt then
					iter Previous (log.cpt - i) (log)
				else if i > log.cpt then
					iter Next (i - log.cpt) (log)
				else
					Ok log
		| PickBranch i ->
				begin match log.cur with
				| Skel (e, Branching (_, sl), k, f) ->
						begin match List.nth_opt sl i with
						| Some s ->
								let cpt = log.cpt+1 in
								let prev = log.cur :: log.prev in
								let cur = Skel (e, s, k, f) in
								Ok {log with cpt; prev; cur}
						| None -> Error ("No branch #" ^ string_of_int i)
						end
				| _ -> Error "Current expression is not a branching"
				end
		| Breakpoint term_name ->
				let rec next log =
					let* log = perform Next log in
					loop log
				and loop log =
					let continue =
						begin match log.cur with
						| CUnspec (u, _, _) ->
								begin match partial_unspec_type u with
								| Arrow _ -> true
								| _ -> term_name <> "*" && partial_unspec_name u <> term_name
								end
						| CSpec (s, _, _) ->
								begin match partial_spec_type s with
								| Arrow _ -> true
								| _ -> term_name <> "*" && partial_spec_name s <> term_name
								end
						| _ -> not (over log.cur)
						end
					in
					if continue then next log else Ok log
				in
				loop log
		| Start ->
				let cpt = 0 in
				let beg = log.beg in
				let cur = beg in
				let prev = [] in
				Ok {log with cpt; cur; prev}
		| FastForward ->
				if over log.cur then Ok log else
				let* log = perform Next log in
				perform FastForward log
		| Drop ->
				let cpt = log.cpt + 1 in
				let prev = log.cur :: log.prev in
				let cur = drop log.cur in
				Ok {log with cpt; prev; cur}
		| Step ->
				let rec compute_step log =
					begin match log.cur with
					| Fail _ -> Ok log
					| Ret (x, ty, MarkStep k, f) ->
							Ok {log with cur = Ret (x, ty, k, f)}
					| _ ->
							let* log = perform Next log in
							compute_step log
					end
				in
				begin match log.cur with
				| Fail _ -> Ok log
				| Ret _ -> Ok log
				| Skel (e, sk, opl, f) ->
						compute_step ({log with cur = Skel (e, sk, MarkStep opl, f)})
				| Term (e, t, opl, f) ->
						compute_step ({log with cur = Term (e, t, MarkStep opl, f)})
				| Apply (f, x, tya, ty, k, fk) ->
						compute_step ({log with cur = Apply (f, x, tya, ty, MarkStep k, fk)})
				| CUnspec (u, k, fk) ->
						compute_step ({log with cur = CUnspec (u, MarkStep k, fk)})
				| CSpec (s, k, fk) ->
						compute_step ({log with cur = CSpec (s, MarkStep k, fk)})
				end
		end

	let compute: type a. a amstate -> a Option.t =
		fun log ->
			begin match perform FastForward log with
			| Error _ -> assert false
			| Ok log ->
					begin match log.cur with
					| Fail None -> None
					| Ret (v, _, Kid, _) -> Some (from_typed_value v)
					| _ -> assert false (* FastForward failed *)
					end
			end

	let rec string_of_bt (bt:'a configuration option): String.t =
		begin match bt with
		| None -> ""
		| Some (Skel (_, s, _, bt')) ->
				"- " ^ print_skeleton s ^ "\n" ^ string_of_bt bt'
		| _ -> failwith "Internal error: backtracking point is not a branching"
		end

		type state_kind = | Term | Skeleton | Value | Fail | Apply

		type printing =
			{ environment: String.t
			; continuation: String.t
			; current: String.t
			; backtrack: String.t
			; counter: Int.t
			; kind: state_kind
			}

	let mkprint environment continuation current backtrack counter kind =
			{ environment ; continuation ; current ; backtrack ; counter ; kind}

	let print_state: type a. a amstate -> printing =
		fun a ->
		begin match a.cur with
		| Fail None ->
				mkprint
				""
				""
				""
				"No result"
				a.cpt
				Fail
		| Fail (Some bt) ->
				mkprint
				""
				""
				"Failure in execution, backtracking"
				(string_of_bt (Some bt))
				a.cpt
				Fail
		| Ret (v, ty, Kid, bt) ->
				mkprint
				""
				""
				(print_typed_value ty v)
				(string_of_bt bt)
				a.cpt
				Value
		| Ret (v, ty, k, bt)->
				mkprint
				(print_env SMap.empty)
				(string_of_cont k)
				(print_typed_value ty v)
				(string_of_bt bt)
				a.cpt
				Value
		| Term (e, t, k, bt) ->
				mkprint (print_env e)
				(string_of_cont k)
				(print_term t)
				(string_of_bt bt)
				a.cpt
				Term
		| Skel (e, sk, k, bt) ->
				mkprint (print_env e)
				(string_of_cont k)
				(print_skeleton sk)
				(string_of_bt bt)
				a.cpt
				Skeleton
		| Apply (f, x, tya, tyb, k, bt) ->
				mkprint
				""
				(string_of_cont k)
				(
					print_typed_value (Arrow (tya, tyb)) f  ^
					" " ^
					print_typed_value tya x)
				(string_of_bt bt)
				a.cpt
				Apply
		| CUnspec (u, k, bt)->
				mkprint
				(print_env SMap.empty)
				(string_of_cont k)
				(print_typed_value (partial_unspec_type u) (VUnspec u))
				(string_of_bt bt)
				a.cpt
				Apply
		| CSpec (s, k, bt)->
				mkprint (print_env SMap.empty)
				(string_of_cont k)
				(print_typed_value (partial_spec_type s) (VSpec s))
				(string_of_bt bt)
				a.cpt
				Apply
		end

	{# apply_spec #}

	let spec_terms = [{# spec_terms #}]
end
