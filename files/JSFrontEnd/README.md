# JS of OCaml front-end

This directory gives the necessary files for the JS of OCaml front-end. You
can use them to have an in-browser debugger of your skeletal semantics

## How to use it

`necrodebug myfile.sk -o backend.ml` creates an OCaml file called `backend.ml`.
this file provides a module type called `PRINT` and a functor called
`MakeDebugger`.
In a new file, that we shall call `instantiation.ml`, create a module `P` matching
the `PRINT` module type, then write the following lines:

``` OCaml
module Debugger = Backend.MakeDebugger(P)
module Interface = Js_of_ocaml_frontend.MakeJSInterface(Debugger)
```

This creates a module `Interface` which defines only one function, `debug`.
Then, create a state `s` to initiate the machine by using one of the functions
provided by `Debugger`, and run `Interface.debug s`.

Then, you need to transform this ml file into JS by running ocaml on your file,
generating a bytefile `instantiation.byte`, and
`js_of_ocaml instantiation.byte -o debugger.js`
to generate a js file. This file should allow the `index.html` file present in
this folder to work.
