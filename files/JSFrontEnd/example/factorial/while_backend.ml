module SMap = Map.Make(String)

module type TYPES = sig
	type ident
	type lit
	type state
	type vint
end

module type SPEC = sig
	include TYPES

	type boolean =
	| True
	| False
	and expr =
	| Const of lit
	| Var of ident
	| Plus of (expr * expr)
	| Equal of (expr * expr)
	| Not of expr
	and stmt =
	| Skip
	| Assign of (ident * expr)
	| Seq of (stmt * stmt)
	| If of (expr * stmt * stmt)
	| While of (expr * stmt)
	and value =
	| Int of vint
	| Bool of boolean

	val add: (vint * vint) -> vint
	val eq: (vint * vint) -> boolean
	val litToVal: lit -> value
	val read: (ident * state) -> value
	val write: (ident * state * value) -> state

	val print_ident: ident -> String.t
	val print_lit: lit -> String.t
	val print_state: state -> String.t
	val print_vint: vint -> String.t

end

module Spec(T:TYPES) = struct
	include T
	type boolean =
	| True
	| False
	and expr =
	| Const of lit
	| Var of ident
	| Plus of (expr * expr)
	| Equal of (expr * expr)
	| Not of expr
	and stmt =
	| Skip
	| Assign of (ident * expr)
	| Seq of (stmt * stmt)
	| If of (expr * stmt * stmt)
	| While of (expr * stmt)
	and value =
	| Int of vint
	| Bool of boolean

	let print_ident _ = "(ident)"
	let print_lit _ = "(lit)"
	let print_state _ = "(state)"
	let print_vint _ = "(vint)"

end

module type PRINT = sig
	include SPEC
	val print_boolean: boolean -> String.t
	val print_expr: expr -> String.t
	val print_ident: ident -> String.t
	val print_lit: lit -> String.t
	val print_state: state -> String.t
	val print_stmt: stmt -> String.t
	val print_value: value -> String.t
	val print_vint: vint -> String.t

end

module Print(S:SPEC) = struct
	include S
	let rec print_boolean =
	begin function
	| True -> "True "
	| False -> "False "
	end
	and print_expr =
	begin function
	| Const v -> "Const " ^ (print_lit v)
	| Var v -> "Var " ^ (print_ident v)
	| Plus v -> "Plus " ^ (let (v0, v1) = v in Format.sprintf "(%s, %s)" (print_expr v0) (print_expr v1))
	| Equal v -> "Equal " ^ (let (v0, v1) = v in Format.sprintf "(%s, %s)" (print_expr v0) (print_expr v1))
	| Not v -> "Not " ^ (print_expr v)
	end
	and print_stmt =
	begin function
	| Skip -> "Skip "
	| Assign v -> "Assign " ^ (let (v0, v1) = v in Format.sprintf "(%s, %s)" (print_ident v0) (print_expr v1))
	| Seq v -> "Seq " ^ (let (v0, v1) = v in Format.sprintf "(%s, %s)" (print_stmt v0) (print_stmt v1))
	| If v -> "If " ^ (let (v0, v1, v2) = v in Format.sprintf "(%s, %s, %s)" (print_expr v0) (print_stmt v1) (print_stmt v2))
	| While v -> "While " ^ (let (v0, v1) = v in Format.sprintf "(%s, %s)" (print_expr v0) (print_stmt v1))
	end
	and print_value =
	begin function
	| Int v -> "Int " ^ (print_vint v)
	| Bool v -> "Bool " ^ (print_boolean v)
	end

end










module type DEBUGGER = functor (P : PRINT) ->
	sig

		(* Main interface for the machine *)

		type _ amstate

		type action =
			| Next | Previous
			| FastForward | Start
			| Step
			| GoTo of Int.t
			| Breakpoint of String.t

		val perform: action -> 'a amstate -> ('a amstate, String.t) Result.t

		val compute : 'a amstate -> 'a Option.t



		(* Initiate the machine *)

		val apply_eval_expr: (P.state * P.expr) -> P.value amstate
		val apply_eval_stmt: (P.state * P.stmt) -> P.state amstate
		val apply_neg: P.boolean -> P.boolean amstate



		type state_kind = | Term | Skeleton | Value | Fail | Apply
		type printing =
			{ environment: String.t
			; continuation: String.t
			; current: String.t
			; backtrack: String.t
			; counter: Int.t
			; kind: state_kind
			}
		val print_state: _ amstate -> printing

	end



module MakeDebugger: DEBUGGER = functor (P:PRINT) -> struct
	module AbstractMachine = struct
		include P
		type _ witness =
		| Unit: Unit.t witness
		| Tuple2: 'a1 witness * 'a2 witness -> ('a1 * 'a2) witness
		| Tuple3: 'a1 witness * 'a2 witness * 'a3 witness -> ('a1 * 'a2 * 'a3) witness
		| Arrow: 'a witness * 'b witness -> ('a -> 'b) witness
		| Boolean: boolean witness
		| Expr: expr witness
		| Ident: ident witness
		| Lit: lit witness
		| State: state witness
		| Stmt: stmt witness
		| Value: value witness
		| Vint: vint witness

		type (_,_) constructor =
		| CInt: (vint, value) constructor
		| CBool: (boolean, value) constructor
		| CSkip: (unit, stmt) constructor
		| CAssign: ((ident * expr), stmt) constructor
		| CSeq: ((stmt * stmt), stmt) constructor
		| CIf: ((expr * stmt * stmt), stmt) constructor
		| CWhile: ((expr * stmt), stmt) constructor
		| CConst: (lit, expr) constructor
		| CVar: (ident, expr) constructor
		| CPlus: ((expr * expr), expr) constructor
		| CEqual: ((expr * expr), expr) constructor
		| CNot: (expr, expr) constructor
		| CTrue: (unit, boolean) constructor
		| CFalse: (unit, boolean) constructor

		type (_,_) field =
		|

		let constr_type: type a b. (a, b) constructor -> b witness =
			begin function
			| CInt -> Value
			| CBool -> Value
			| CSkip -> Stmt
			| CAssign -> Stmt
			| CSeq -> Stmt
			| CIf -> Stmt
			| CWhile -> Stmt
			| CConst -> Expr
			| CVar -> Expr
			| CPlus -> Expr
			| CEqual -> Expr
			| CNot -> Expr
			| CTrue -> Boolean
			| CFalse -> Boolean
			end

		let field_type: type a b. (a, b) field -> b witness =
			begin function
			| _ -> .
			end

		let untuple2_type: type a1 a2. (a1 * a2) witness
			-> a1 witness * a2 witness=
			begin function
			| Tuple2 (ty1, ty2) -> (ty1, ty2)
			| _ -> assert false
			end

		let untuple3_type: type a1 a2 a3. (a1 * a2 * a3) witness
			-> a1 witness * a2 witness * a3 witness=
			begin function
			| Tuple3 (ty1, ty2, ty3) -> (ty1, ty2, ty3)
			| _ -> assert false
			end

		let constr_compute : type a b. (a,b) constructor -> a -> b =
			fun c a ->
			begin match c with
			| CInt -> Int a
			| CBool -> Bool a
			| CSkip -> let () = a in Skip
			| CAssign -> Assign a
			| CSeq -> Seq a
			| CIf -> If a
			| CWhile -> While a
			| CConst -> Const a
			| CVar -> Var a
			| CPlus -> Plus a
			| CEqual -> Equal a
			| CNot -> Not a
			| CTrue -> let () = a in True
			| CFalse -> let () = a in False
			end

		let field_compute : type a b. (a,b) field -> a -> b =
			fun f _ ->
			begin match f with
			| _ -> .
			end

		let remove_constr : type a b. (a,b) constructor -> b -> a Option.t =
			fun c a ->
			begin match c, a with
			| CInt, Int a -> Some a
			| CBool, Bool a -> Some a
			|CSkip, Skip -> Some ()
			| CAssign, Assign a -> Some a
			| CSeq, Seq a -> Some a
			| CIf, If a -> Some a
			| CWhile, While a -> Some a
			| CConst, Const a -> Some a
			| CVar, Var a -> Some a
			| CPlus, Plus a -> Some a
			| CEqual, Equal a -> Some a
			| CNot, Not a -> Some a
			|CTrue, True -> Some ()
			|CFalse, False -> Some ()
			| _, _ -> None
			end

		type _ spec =
			| S_eval_expr: ((state * expr) -> value) spec
			| S_eval_stmt: ((state * stmt) -> state) spec
			| S_neg: (boolean -> boolean) spec

		type _ unspec =
			| U_add: ((vint * vint) -> vint) unspec
			| U_eq: ((vint * vint) -> boolean) unspec
			| U_litToVal: (lit -> value) unspec
			| U_read: ((ident * state) -> value) unspec
			| U_write: ((ident * state * value) -> state) unspec

		let spec_type: type a. a spec -> a witness =
			fun s ->
			begin match s with
			| S_eval_expr -> Arrow (Tuple2 (State, Expr), Value)
			| S_eval_stmt -> Arrow (Tuple2 (State, Stmt), State)
			| S_neg -> Arrow (Boolean, Boolean)
			end

		let unspec_type: type a. a unspec -> a witness =
			fun u ->
			begin match u with
			| U_add -> Arrow (Tuple2 (Vint, Vint), Vint)
			| U_eq -> Arrow (Tuple2 (Vint, Vint), Boolean)
			| U_litToVal -> Arrow (Lit, Value)
			| U_read -> Arrow (Tuple2 (Ident, State), Value)
			| U_write -> Arrow (Tuple3 (Ident, State, Value), State)
			end

		let unspec_compute: type a. a unspec -> a =
			fun u ->
			begin match u with
			| U_add -> P.add
			| U_eq -> P.eq
			| U_litToVal -> P.litToVal
			| U_read -> P.read
			| U_write -> P.write
			end

		exception MatchEmpty
		let rec string_of_wit: type a. a witness -> String.t =
			begin function
			| Arrow (i, o) ->
					Format.sprintf "(%s → %s)" (string_of_wit i) (string_of_wit o)
			| Unit -> "()"
			| Tuple2 (a0, a1) ->
				Format.sprintf "(%s, %s)"
				(string_of_wit a0) (string_of_wit a1)
			| Tuple3 (a0, a1, a2) ->
				Format.sprintf "(%s, %s, %s)"
				(string_of_wit a0) (string_of_wit a1) (string_of_wit a2)
			| Boolean -> "boolean"
			| Expr -> "expr"
			| Ident -> "ident"
			| Lit -> "lit"
			| State -> "state"
			| Stmt -> "stmt"
			| Value -> "value"
			| Vint -> "vint"
			end

		let rec print_val: type a. a witness -> a -> String.t =
			fun w v ->
			begin match w with
			| Unit -> "()"
			| Arrow _ -> "(fun)"
			| Tuple2 (ty0, ty1) ->
				let (v0, v1) = v in
				Format.sprintf "(%s, %s)"
				(print_val ty0 v0) (print_val ty1 v1)
			| Tuple3 (ty0, ty1, ty2) ->
				let (v0, v1, v2) = v in
				Format.sprintf "(%s, %s, %s)"
				(print_val ty0 v0) (print_val ty1 v1) (print_val ty2 v2)
			| Boolean -> print_boolean v
			| Expr -> print_expr v
			| Ident -> print_ident v
			| Lit -> print_lit v
			| State -> print_state v
			| Stmt -> print_stmt v
			| Value -> print_value v
			| Vint -> print_vint v
			end

		let print_constr: type a b. (a, b) constructor -> String.t =
			fun c ->
			begin match c with
			| CInt -> "Int"
			| CBool -> "Bool"
			| CSkip -> "Skip"
			| CAssign -> "Assign"
			| CSeq -> "Seq"
			| CIf -> "If"
			| CWhile -> "While"
			| CConst -> "Const"
			| CVar -> "Var"
			| CPlus -> "Plus"
			| CEqual -> "Equal"
			| CNot -> "Not"
			| CTrue -> "True"
			| CFalse -> "False"
			end

		let print_field: type a b. (a, b) field -> String.t =
			fun f ->
			begin match f with
			| _ -> .
			end

		let print_spec: type a. a spec -> String.t =
			fun s ->
			begin match s with
			| S_eval_expr -> "eval_expr"
			| S_eval_stmt -> "eval_stmt"
			| S_neg -> "neg"
			end

		let print_unspec: type a. a unspec -> String.t =
			fun s ->
			begin match s with
			| U_add -> "add"
			| U_eq -> "eq"
			| U_litToVal -> "litToVal"
			| U_read -> "read"
			| U_write -> "write"
			end

		module type TYPET = sig type _ t end
		module Caster(T:TYPET) = struct
			type (_, _) eq = Refl: ('a, 'a) eq
			let rec geteq: type a b. a witness -> b witness -> (a, b) eq =
				fun wa wb ->
				begin match wa, wb with
				| Unit, Unit -> Refl
				| Arrow (ia, oa), Arrow (ib, ob) ->
						let eqi = geteq ia ib in
						let eqo = geteq oa ob in
						begin match eqi, eqo with Refl, Refl -> Refl end
				| Tuple2 (tya0, tya1), Tuple2 (tyb0, tyb1) ->
						let eq0 = geteq tya0 tyb0 in
						let eq1 = geteq tya1 tyb1 in
						begin match eq0, eq1 with Refl, Refl -> Refl end
				| Tuple3 (tya0, tya1, tya2), Tuple3 (tyb0, tyb1, tyb2) ->
						let eq0 = geteq tya0 tyb0 in
						let eq1 = geteq tya1 tyb1 in
						let eq2 = geteq tya2 tyb2 in
						begin match eq0, eq1, eq2 with Refl, Refl, Refl -> Refl end
				| Boolean, Boolean -> Refl
				| Expr, Expr -> Refl
				| Ident, Ident -> Refl
				| Lit, Lit -> Refl
				| State, State -> Refl
				| Stmt, Stmt -> Refl
				| Value, Value -> Refl
				| Vint, Vint -> Refl
				| _, _ -> Format.kasprintf failwith "cannot match type %s with type %s" (string_of_wit wa)
				(string_of_wit wb)
				end
			let cast: type a b. a witness -> b witness -> a T.t -> b T.t
			= fun wa wb v ->
				let eq: (a, b) eq = geteq wa wb in
				match eq with | Refl -> v
		end


		type 'a unspec_app =
		| PUUnspec : 'a unspec -> 'a unspec_app
		| PUApp  : ('a -> 'b) unspec_app * 'a witness * 'a typed_value -> 'b
		unspec_app

		and 'a spec_app =
		| PSSpec : 'a spec -> 'a spec_app
		| PSApp  : ('a -> 'b) spec_app * 'a witness * 'a typed_value -> 'b spec_app

		and 'a typed_value =
		| VTuple2: 'a1 typed_value * 'a2 typed_value -> ('a1 * 'a2) typed_value
		| VTuple3: 'a1 typed_value * 'a2 typed_value * 'a3 typed_value -> ('a1 * 'a2 * 'a3) typed_value
		| Value: 'a -> 'a typed_value
		| Term: 'a term -> 'a typed_value
		| Clos: env * 'a pattern * 'b skeleton -> ('a -> 'b) typed_value
		| VUnspec: 'a unspec_app -> 'a typed_value
		| VSpec: 'a spec_app -> 'a typed_value

		and value =
			Pack: 'a typed_value * 'a witness -> value

		and 'a typed_value_record =
		| VNil: 'a typed_value_record
		| VCons: ('a, 'b) field * 'b typed_value * 'a typed_value_record -> 'a typed_value_record

		and env = value SMap.t

		and 'a pattern_record =
		| PNil: 'a pattern_record
		| PCons: ('a, 'b) field * 'b pattern * 'a pattern_record -> 'a pattern_record

		and _ pattern =
		| PWild : 'a witness -> 'a pattern
		| Ptt: Unit.t pattern
		| PVar : String.t * 'a witness -> 'a pattern
		| PConstr: ('a, 'b) constructor * 'a pattern -> 'b pattern
		| PRecord: 'a witness * 'a pattern_record -> 'a pattern
		| POr : 'a pattern * 'a pattern -> 'a pattern
		| PTuple2: 'a0 pattern * 'a1 pattern -> ('a0 * 'a1) pattern
		| PTuple3: 'a0 pattern * 'a1 pattern * 'a2 pattern -> ('a0 * 'a1 * 'a2) pattern

		and 'a term_record =
		| TNil: 'a term_record
		| TCons: ('a, 'b) field * 'b term * 'a term_record -> 'a term_record

		and _ term =
		| Var: String.t * 'a witness -> 'a term
		| Spec: 'a spec -> 'a term
		| Unspec: 'a unspec -> 'a term
		| Constr : ('a, 'b) constructor * 'a term -> 'b term
		| Tt : Unit.t term
		| Tuple2: 'a0 term * 'a1 term -> ('a0 * 'a1) term
		| Tuple3: 'a0 term * 'a1 term * 'a2 term -> ('a0 * 'a1 * 'a2) term
		| Func : 'a pattern * 'b skeleton -> ('a -> 'b) term
		| Field: 'a term * ('a, 'b) field -> 'b term
		| Nth2_1: ('a1 * 'a2) term -> 'a1 term
		| Nth2_2: ('a1 * 'a2) term -> 'a2 term
		| Nth3_1: ('a1 * 'a2 * 'a3) term -> 'a1 term
		| Nth3_2: ('a1 * 'a2 * 'a3) term -> 'a2 term
		| Nth3_3: ('a1 * 'a2 * 'a3) term -> 'a3 term
		| RecMake: 'a witness * 'a term_record -> 'a term
		| RecSet: 'a term * 'a term_record -> 'a term

		and _ skeleton =
		| Return: 'a term -> 'a skeleton
		| Branching: 'a witness * 'a skeleton List.t -> 'a skeleton
		| Match: 'a skeleton * ('a pattern * 'b skeleton) List.t -> 'b skeleton
		| LetIn: 'a pattern * 'a skeleton * 'b skeleton -> 'b skeleton
		| Apply: ('a -> 'b) term * 'a term -> 'b skeleton
		(* exists: LATER *)

		let rec partial_unspec_name: type a. a unspec_app -> String.t = fun u ->
			begin match u with
			| PUApp (x, _, _) -> partial_unspec_name x
			| PUUnspec u -> print_unspec u
			end

		let rec partial_spec_name: type a. a spec_app -> String.t = fun u ->
			begin match u with
			| PSApp (x, _, _) -> partial_spec_name x
			| PSSpec u -> print_spec u
			end

		let rec pattern_type: type a. a pattern -> a witness =
			fun p ->
			begin match p with
			| PWild ty -> ty
			| Ptt -> Unit
			| PVar (_, ty) -> ty
			| POr (p, _) -> pattern_type p
			| PConstr (c, _) -> constr_type c
			| PRecord (ty, _) -> ty
			| PTuple2 (p0, p1) -> Tuple2 (pattern_type p0, pattern_type p1)
			| PTuple3 (p0, p1, p2) -> Tuple3 (pattern_type p0, pattern_type p1, pattern_type p2)
			end

		let rec skel_type: type a. a skeleton -> a witness =
			fun s ->
			begin match s with
			| Return t -> term_type t
			| Branching (ty, _) -> ty
			| Match (_, []) -> failwith "empty matchings are not allowed"
			| Match (_, (_, s) :: _) -> skel_type s
			| LetIn (_, _, s) -> skel_type s
			| Apply (f, _) ->
					begin match term_type f with
					| Arrow (_, b) -> b
					| _ -> failwith "Can only apply arrow types"
					end
			end

		and term_type: type a. a term -> a witness =
			fun t ->
			begin match t with
			| Var (_, ty) -> ty
			| Constr (c, _) -> constr_type c
			| Tt -> Unit
			| Tuple2 (t0, t1) -> Tuple2 (term_type t0, term_type t1)
			| Tuple3 (t0, t1, t2) -> Tuple3 (term_type t0, term_type t1, term_type t2)
			| Func (p, s) -> Arrow (pattern_type p, skel_type s)
			| Spec s -> spec_type s
			| Unspec u -> unspec_type u
			| Field _ -> .
			| Nth2_1 t ->
				let (ty, _) = untuple2_type (term_type t) in ty
			| Nth2_2 t ->
				let (_, ty) = untuple2_type (term_type t) in ty
			| Nth3_1 t ->
				let (ty, _, _) = untuple3_type (term_type t) in ty
			| Nth3_2 t ->
				let (_, ty, _) = untuple3_type (term_type t) in ty
			| Nth3_3 t ->
				let (_, _, ty) = untuple3_type (term_type t) in ty
			| RecMake (ty, _) -> ty
			| RecSet (t, _) -> term_type t
			end

		and partial_unspec_type: type a. a unspec_app -> a witness =
			fun u ->
			begin match u with
			| PUUnspec t -> unspec_type t
			| PUApp (f, _, _) ->
					begin match partial_unspec_type f with
					| Arrow (_, b) -> b
					| _ -> failwith "Can only apply arrow types"
					end
			end

		and partial_spec_type: type a. a spec_app -> a witness =
			fun u ->
			begin match u with
			| PSSpec t -> spec_type t
			| PSApp (f, _, _) ->
					begin match partial_spec_type f with
					| Arrow (_, b) -> b
					| _ -> failwith "Can only apply arrow types"
					end
			end
		let spec_compute: type a. a spec -> a term =
			fun s ->
			begin match s with
			| S_eval_expr -> Func (PTuple2 (PVar ("s", State), PVar ("e", Expr)), Match (Return (Var ("e", Expr)), [(PConstr (CConst, PVar ("i", Lit)), Apply (Unspec U_litToVal, Var ("i", Lit))); (PConstr (CVar, PVar ("x", Ident)), Apply (Unspec U_read, Tuple2 (Var ("x", Ident), Var ("s", State)))); (PConstr (CPlus, PTuple2 (PVar ("e1", Expr), PVar ("e2", Expr))), LetIn (PConstr (CInt, PVar ("i1", Vint)), Apply (Spec S_eval_expr, Tuple2 (Var ("s", State), Var ("e1", Expr))), LetIn (PConstr (CInt, PVar ("i2", Vint)), Apply (Spec S_eval_expr, Tuple2 (Var ("s", State), Var ("e2", Expr))), LetIn (PVar ("i", Vint), Apply (Unspec U_add, Tuple2 (Var ("i1", Vint), Var ("i2", Vint))), Return (Constr (CInt, Var ("i", Vint))))))); (PConstr (CEqual, PTuple2 (PVar ("e1", Expr), PVar ("e2", Expr))), LetIn (PConstr (CInt, PVar ("i1", Vint)), Apply (Spec S_eval_expr, Tuple2 (Var ("s", State), Var ("e1", Expr))), LetIn (PConstr (CInt, PVar ("i2", Vint)), Apply (Spec S_eval_expr, Tuple2 (Var ("s", State), Var ("e2", Expr))), LetIn (PVar ("b", Boolean), Apply (Unspec U_eq, Tuple2 (Var ("i1", Vint), Var ("i2", Vint))), Return (Constr (CBool, Var ("b", Boolean))))))); (PConstr (CNot, PVar ("e", Expr)), LetIn (PConstr (CBool, PVar ("b", Boolean)), Apply (Spec S_eval_expr, Tuple2 (Var ("s", State), Var ("e", Expr))), LetIn (PVar ("b'", Boolean), Apply (Spec S_neg, Var ("b", Boolean)), Return (Constr (CBool, Var ("b'", Boolean))))))]))
			| S_eval_stmt -> Func (PTuple2 (PVar ("s", State), PVar ("t", Stmt)), Match (Return (Var ("t", Stmt)), [(PConstr (CSkip, Ptt), Return (Var ("s", State))); (PConstr (CAssign, PTuple2 (PVar ("x", Ident), PVar ("e", Expr))), LetIn (PVar ("v", Value), Apply (Spec S_eval_expr, Tuple2 (Var ("s", State), Var ("e", Expr))), Apply (Unspec U_write, Tuple3 (Var ("x", Ident), Var ("s", State), Var ("v", Value))))); (PConstr (CSeq, PTuple2 (PVar ("t1", Stmt), PVar ("t2", Stmt))), LetIn (PVar ("s'", State), Apply (Spec S_eval_stmt, Tuple2 (Var ("s", State), Var ("t1", Stmt))), Apply (Spec S_eval_stmt, Tuple2 (Var ("s'", State), Var ("t2", Stmt))))); (PConstr (CIf, PTuple3 (PVar ("cond", Expr), PVar ("true", Stmt), PVar ("false", Stmt))), LetIn (PConstr (CBool, PVar ("b", Boolean)), Apply (Spec S_eval_expr, Tuple2 (Var ("s", State), Var ("cond", Expr))), Match (Return (Var ("b", Boolean)), [(PConstr (CTrue, Ptt), Apply (Spec S_eval_stmt, Tuple2 (Var ("s", State), Var ("true", Stmt)))); (PConstr (CFalse, Ptt), Apply (Spec S_eval_stmt, Tuple2 (Var ("s", State), Var ("false", Stmt))))]))); (PConstr (CWhile, PTuple2 (PVar ("cond", Expr), PVar ("t'", Stmt))), LetIn (PConstr (CBool, PVar ("b", Boolean)), Apply (Spec S_eval_expr, Tuple2 (Var ("s", State), Var ("cond", Expr))), Match (Return (Var ("b", Boolean)), [(PConstr (CTrue, Ptt), LetIn (PVar ("s'", State), Apply (Spec S_eval_stmt, Tuple2 (Var ("s", State), Var ("t'", Stmt))), Apply (Spec S_eval_stmt, Tuple2 (Var ("s'", State), Var ("t", Stmt))))); (PConstr (CFalse, Ptt), Return (Var ("s", State)))])))]))
			| S_neg -> Func (PVar ("b", Boolean), Match (Return (Var ("b", Boolean)), [(PConstr (CTrue, Ptt), Return (Constr (CFalse, Tt))); (PConstr (CFalse, Ptt), Return (Constr (CTrue, Tt)))]))
			end




		let cast_term: type a b. a witness -> b witness -> a term -> b term =
			fun a b v -> let module C = Caster(struct type 'a t = 'a term end) in C.cast a b v

		let cast_val: type a b. a witness -> b witness -> a typed_value -> b typed_value =
			fun a b v -> let module C = Caster(struct type 'a t = 'a typed_value end) in C.cast a b v

		let append: type a b.
				(a, b) field -> b typed_value ->
				a typed_value_record ->
				a typed_value_record =
			fun f v fvl ->
			begin match fvl with
			| VNil -> VCons (f, v, VNil)
			| VCons _ -> .
			end

		let get_field_value:
				type a b. (a, b) field -> a typed_value_record -> b typed_value =
			fun f fvl ->
			begin match f, fvl with
			| _, VNil -> failwith ("unbound field " ^ print_field f)
			
			| _, VCons _ -> .
			end

		let rec record_compute:
				type a. a witness -> a typed_value_record -> a typed_value =
			fun w _ ->
				begin match w with
				
				| _ -> failwith ("Type " ^ (string_of_wit w) ^ " is not a record type")
				end

		(* field_reset _ v f w is v ← (f=w) *)
		and field_reset: type a b.
				a typed_value -> (a, b) field -> b typed_value -> a typed_value =
			fun _ f _ ->
			begin match f with
			| _ -> .
			end


		(* record_reset _ v fvl is v ← (fvl) *)
		and record_reset: type a.
				a witness -> a typed_value -> a typed_value_record -> a typed_value =
			fun _ v fvl ->
				begin match fvl with
				| VNil -> v
				| VCons _ -> .
				end


		and extend_env: type a. env -> a pattern -> a typed_value -> env Option.t = fun e p v ->
			begin match p with
			| PWild _ -> Some e
			| PVar (x, ty) -> Some (SMap.add x (Pack (v, ty)) e)
			| PConstr (c, p) ->
				begin match remove_constr c (from_typed_value v) with
				| Some w -> extend_env e p (Value w)
				| None -> None
				end
			| Ptt -> Some e
			| POr (p1, p2) ->
					begin match extend_env e p1 v with
					| Some e' -> Some e'
					| None -> extend_env e p2 v
					end
			| PTuple2 (p0, p1) ->
				let (v0, v1) = untuple2 v in
				let (let*) = Option.bind in
				let* e0 = extend_env e p0 v0 in
				let* e1 = extend_env e0 p1 v1 in
				Some e1
			| PTuple3 (p0, p1, p2) ->
				let (v0, v1, v2) = untuple3 v in
				let (let*) = Option.bind in
				let* e0 = extend_env e p0 v0 in
				let* e1 = extend_env e0 p1 v1 in
				let* e2 = extend_env e1 p2 v2 in
				Some e2
			| PRecord (_, pl) -> extend_env_rec e pl v
			end

		and extend_env_rec:
				type a. env -> a pattern_record -> a typed_value -> env Option.t =
			fun e pl _ ->
			begin match pl with
			| PNil -> Some e
			| PCons _ -> .
			end

		and from_typed_value: type a. a typed_value -> a = fun x ->
			begin match x with
			| VTuple2 (v1, v2) -> (from_typed_value v1, from_typed_value v2)
			| VTuple3 (v1, v2, v3) -> (from_typed_value v1, from_typed_value v2, from_typed_value v3)
			| Value x -> x
			| Clos (e, p, s) ->
					fun x ->
						begin match extend_env e p (Value x) with
						| None -> failwith "pattern matching failed"
						| Some e' -> from_typed_value (eval_skel e' s)
						end
			| Term t -> from_typed_value (eval_term SMap.empty t)
			| VUnspec (PUUnspec u) -> unspec_compute u
			| VUnspec (PUApp (f, _, x)) -> from_typed_value (VUnspec f) @@ from_typed_value x
			| VSpec (PSSpec s) -> from_typed_value @@ Term (spec_compute s)
			| VSpec (PSApp (f, _, x)) ->
					from_typed_value (VSpec f) @@ from_typed_value x
			end

		and partial_unspec_compute: type a. a unspec_app -> a =
			fun u ->
			begin match u with
			| PUUnspec u -> unspec_compute u
			| PUApp (f, _, x) -> partial_unspec_compute f @@ from_typed_value x
			end

		and untuple2: type a1 a2.
			(a1 * a2) typed_value ->
				a1 typed_value * a2 typed_value =
			begin function
			| VTuple2 (v1, v2) -> (v1, v2)
			| v ->
				let (a1, a2) = from_typed_value v in
				(Value a1, Value a2)
			end

		and untuple3: type a1 a2 a3.
			(a1 * a2 * a3) typed_value ->
				a1 typed_value * a2 typed_value * a3 typed_value =
			begin function
			| VTuple3 (v1, v2, v3) -> (v1, v2, v3)
			| v ->
				let (a1, a2, a3) = from_typed_value v in
				(Value a1, Value a2, Value a3)
			end

		and eval_term : type a. env -> a term -> a typed_value = fun e t ->
			begin match t with
			| Constr (c, x) ->
				Value (constr_compute c @@ from_typed_value @@ eval_term e x)
			| Tt -> Value ()
			| RecMake (ty, ftl) ->
					let fvl = eval_term_record e ftl in
					record_compute ty fvl
			| RecSet (t, ftl) ->
					let v = eval_term e t in
					let fvl = eval_term_record e ftl in
					record_reset (term_type t) v fvl
			| Field _ -> .
			| Tuple2 (t0, t1) -> Value (
				from_typed_value (eval_term e t0),
				from_typed_value (eval_term e t1))
			| Tuple3 (t0, t1, t2) -> Value (
				from_typed_value (eval_term e t0),
				from_typed_value (eval_term e t1),
				from_typed_value (eval_term e t2))
			| Nth2_1 t ->
				let (v, _) = untuple2 (eval_term e t) in v
			| Nth2_2 t ->
				let (_, v) = untuple2 (eval_term e t) in v
			| Nth3_1 t ->
				let (v, _, _) = untuple3 (eval_term e t) in v
			| Nth3_2 t ->
				let (_, v, _) = untuple3 (eval_term e t) in v
			| Nth3_3 t ->
				let (_, _, v) = untuple3 (eval_term e t) in v
			| Var (x, typ) ->
				begin match SMap.find_opt x e with
				| None -> failwith (x ^ " was not found in the environment.\n" ^
					"Current environment:\n" ^ print_env e)
				| Some (Pack (v, tyv)) -> cast_val tyv typ v
				end
			| Func (p, sk) -> Clos (e, p, sk)
			| Spec s -> Term (spec_compute s)
			| Unspec s -> Value (unspec_compute s)
			end

		and eval_term_record: type a. env -> a term_record -> a typed_value_record =
			fun _ ftl ->
			begin match ftl with
			| TNil -> VNil
			| TCons _ -> .
			end

		and print_typed_value: type a. a witness -> a typed_value -> String.t =
			fun w v ->
			begin match v with
			| VTuple2 (v1, v2) ->
				begin match w with
				| Tuple2 (ty1, ty2) ->
					Format.sprintf "(%s, %s)"
					(print_typed_value ty1 v1) (print_typed_value ty2 v2)
				| _ -> failwith "Impossible: printing tuple2"
				end
			| VTuple3 (v1, v2, v3) ->
				begin match w with
				| Tuple3 (ty1, ty2, ty3) ->
					Format.sprintf "(%s, %s, %s)"
					(print_typed_value ty1 v1) (print_typed_value ty2 v2) (print_typed_value ty3 v3)
				| _ -> failwith "Impossible: printing tuple3"
				end
			| Value v -> print_val w v
			| Term t -> print_term t
			| Clos (_, p, s) ->
				Format.sprintf "λ%s·%s" (print_pattern p) (print_skeleton s)
			| VUnspec (PUUnspec u) -> print_unspec u
			| VUnspec (PUApp (f, tyb, x)) ->
				Format.sprintf "%s %s"
				(print_typed_value (partial_unspec_type f) (VUnspec f))
				(print_typed_value tyb x)
				| VSpec (PSSpec u) -> print_spec u
			| VSpec (PSApp (f, tyb, x)) ->
				Format.sprintf "%s %s"
				(print_typed_value (partial_spec_type f) (VSpec f))
				(print_typed_value tyb x)
			end

		and print_typed_value_record: type a. a typed_value_record -> String.t = fun fvl ->
			begin match fvl with
			| VNil -> ""
			| VCons _ -> .
			end

		and print_skeleton: type a. ?indent:Int.t -> a skeleton -> String.t =
			fun ?(indent=0) s ->
			let open Format in
			let nl = "\n" ^ String.make indent ' ' in
			begin match s with
			| Branching (_, []) -> "branch end"
			| Branching (_, sl) ->
					let sep = nl ^ "or" ^ nl ^ "  " in
					sprintf "branch%s  %s%send"
					nl (String.concat sep (List.map (print_skeleton ~indent:(indent+2)) sl)) nl
			| Match (s, psl) ->
					sprintf "match %s with%s%send" (print_skeleton ~indent s)
					nl (List.map (fun (p,s) ->
						sprintf "| %s -> %s%s" (print_pattern p) (print_skeleton
						~indent:(indent+2) s) nl) psl |> String.concat "")
			| LetIn (p, s1, s2) ->
					sprintf "let %s = %s in%s%s" (print_pattern p)
					(print_skeleton ~indent s1) nl (print_skeleton ~indent s2)
			| Apply (f, x) -> sprintf "%s %s"
					(print_term ~indent ~protect:true f)
					(print_term ~indent ~protect:true x)
			| Return t -> print_term ~indent t
			end

		and print_term: type a. ?indent:Int.t -> ?protect:Bool.t -> a term -> String.t =
			fun ?(indent=0) ?(protect=false) t ->
			let open Format in
			let nl =
				"\n" ^ String.concat "" (List.init indent (Fun.const " ")) in
			begin match t with
			| Var (x, _) -> x
			| Spec s -> print_spec s
			| Unspec u -> print_unspec u
			| Constr (c, t) ->
				if protect then
					sprintf "(%s %s)" (print_constr c) (print_term ~indent ~protect t)
				else
					sprintf "%s %s" (print_constr c) (print_term ~indent ~protect:true t)
			| Tt -> "()"
			| Tuple2 (t0, t1) ->
				Format.sprintf "(%s, %s)"
				(print_term ~indent ~protect t0) (print_term ~indent ~protect t1)
			| Tuple3 (t0, t1, t2) ->
				Format.sprintf "(%s, %s, %s)"
				(print_term ~indent ~protect t0) (print_term ~indent ~protect t1) (print_term ~indent ~protect t2)
			| Func (p, s) when protect ->
					sprintf "(λ %s →%s  %s)"
					(print_pattern p) nl (print_skeleton ~indent:(indent+2) s)
			| Func (p, s) ->
					sprintf "λ %s →%s  %s"
					(print_pattern p) nl (print_skeleton ~indent:(indent+2) s)
			| Field _ -> .
			| Nth2_1 t ->
					Format.sprintf "%s.1" (print_term ~indent ~protect t)
			| Nth2_2 t ->
					Format.sprintf "%s.2" (print_term ~indent ~protect t)
			| Nth3_1 t ->
					Format.sprintf "%s.1" (print_term ~indent ~protect t)
			| Nth3_2 t ->
					Format.sprintf "%s.2" (print_term ~indent ~protect t)
			| Nth3_3 t ->
					Format.sprintf "%s.3" (print_term ~indent ~protect t)
			| RecMake (_, ftl) ->
					Format.sprintf "(%s)" (print_term_record ~indent ftl)
			| RecSet (t, ftl) ->
					Format.sprintf "%s ← (%s)" (print_term ~indent t) (print_term_record
					~indent ftl)
			end
		and print_term_record: type a. ?indent:Int.t -> a term_record -> String.t =
			fun ?(indent=0) ftl -> ignore indent;
			begin match ftl with
			| TNil -> ""
			| TCons _ -> .
			end

		and print_pattern: type a. ?protect:Bool.t -> a pattern -> String.t = fun
			?(protect=false) p ->
			let open Format in
			begin match p with
			| PWild _ -> "_"
			| PVar (x, _) -> x
			| PConstr (c, p) ->
					sprintf "%s %s" (print_constr c) (print_pattern ~protect:true p)
					|> if protect then sprintf "(%s)" else Fun.id
			| PTuple2 (p0, p1) ->
				Format.sprintf "(%s, %s)"
				(print_pattern p0) (print_pattern p1)
			| PTuple3 (p0, p1, p2) ->
				Format.sprintf "(%s, %s, %s)"
				(print_pattern p0) (print_pattern p1) (print_pattern p2)
			| POr (p1, p2) when protect ->
					sprintf "(%s | %s)" (print_pattern p1) (print_pattern p2)
			| POr (p1, p2) ->
					sprintf "%s | %s" (print_pattern p1) (print_pattern p2)
			| Ptt -> "()"
			| PRecord (_, fpl) ->
					sprintf "(%s)" (print_pattern_record fpl)
			end
		and print_pattern_record: type a. a pattern_record -> String.t = fun fpl ->
			begin match fpl with
			| PNil -> ""
			| PCons _ -> .
			end

		and print_env e =
			let rec aux l =
				begin match l with
				| [] -> ""
				| (name, (Pack (v, w))) :: q ->
						Format.sprintf "- %s ← %s"
						name (print_typed_value w v) ^ "\n" ^
						aux q
				end
			in aux (SMap.bindings e)


		and eval_skel : type a. env -> a skeleton -> a typed_value = fun e sk ->
			begin match sk with
			| Return t -> eval_term e t
			| Branching (_, sl) ->
					let s1 = List.hd sl in
					eval_skel e s1
			| Match (_, []) -> raise MatchEmpty
			| Match (t, (p, s) :: psl) ->
					let v = eval_skel e t in
					begin match extend_env e p v with
					| None -> eval_skel e (Match (t, psl))
					| Some e' -> eval_skel e' s
					end
			| LetIn (p, b, s) ->
					let v = eval_skel e b in
					begin match extend_env e p v with
					| None -> failwith "pattern matching not successful"
					| Some e' -> eval_skel e' s
					end
			| Apply (f, x) ->
					let f = eval_term e f in
					let x = eval_term e x in
					apply f x
			end

			and apply: type a b. (a -> b) typed_value -> a typed_value -> b typed_value =
				fun f x -> Value ((from_typed_value f) (from_typed_value x))

		type ('a, 'b) cont = (* continuation with a hole of type 'a, which represents
		a value of type 'b *)
			| MarkStep: ('a, 'b) cont -> ('a, 'b) cont
			| Kid: ('a, 'a) cont (* identity continuation *)
			| Kconstr: ('a, 'b) constructor * ('b, 'c) cont -> ('a, 'c) cont
			| Ktuple2_1:
				env
				* 'a1 term
				* ('a0 * 'a1, 'b) cont
					-> ('a0, 'b) cont
			| Ktuple2_2:
				'a0 typed_value * 'a0 witness
				* ('a0 * 'a1, 'b) cont
					-> ('a1, 'b) cont
			| Ktuple3_1:
				env
				* 'a1 term
				* 'a2 term
				* ('a0 * 'a1 * 'a2, 'b) cont
					-> ('a0, 'b) cont
			| Ktuple3_2:
				'a0 typed_value * 'a0 witness
				* env
				* 'a2 term
				* ('a0 * 'a1 * 'a2, 'b) cont
					-> ('a1, 'b) cont
			| Ktuple3_3:
				'a0 typed_value * 'a0 witness
				* 'a1 typed_value * 'a1 witness
				* ('a0 * 'a1 * 'a2, 'b) cont
					-> ('a2, 'b) cont
			| Knth2_1:
				('a1, 'b) cont -> ('a1 * 'a2, 'b) cont
			| Knth2_2:
				('a2, 'b) cont -> ('a1 * 'a2, 'b) cont
			| Knth3_1:
				('a1, 'b) cont -> ('a1 * 'a2 * 'a3, 'b) cont
			| Knth3_2:
				('a2, 'b) cont -> ('a1 * 'a2 * 'a3, 'b) cont
			| Knth3_3:
				('a3, 'b) cont -> ('a1 * 'a2 * 'a3, 'b) cont
			| Kmatch: env * ('a pattern * 'b skeleton) List.t * ('b, 'c) cont -> ('a, 'c) cont
			| Klet: env * 'a pattern * 'b skeleton * ('b, 'c) cont -> ('a, 'c) cont
			| Kapply_1: env * 'a term * ('b, 'c) cont -> ('a -> 'b, 'c) cont
			| Kapply_2: ('a -> 'b) typed_value * ('a -> 'b) witness * ('b, 'c) cont -> ('a, 'c) cont
			| Kapply_3: 'a witness * 'a typed_value * ('b, 'c) cont -> ('a -> 'b, 'c) cont
			| Kfield: ('a, 'b) field * ('b, 'c) cont -> ('a, 'c) cont
			| Krecmake: 'b witness * env * 'b typed_value_record * ('b, 'a) field * 'b term_record * ('b, 'c) cont -> ('a, 'c) cont
			(* Kset (e, ftl, k)  is   k [ □ ← ftl(in env e) ]   *)
			| Kset: env * 'a term_record * ('a, 'b) cont -> ('a, 'b) cont
			(* Kset2 (v, _, f, k)  is   k [ v ← (f ← □) ]   *)
			| Kset2: 'a typed_value * 'a witness * ('a, 'b) field * ('a, 'c) cont -> ('b, 'c) cont


		let rec string_of_cont: type a b. (a, b) cont -> String.t =
			fun k ->
			let open Format in
			begin match k with
			| MarkStep k -> string_of_cont k
			| Kid -> ""
			| Kconstr (c, k) -> sprintf
				"- %s □\n%s" (print_constr c) (string_of_cont k)
			| Ktuple2_1 (_, t2, k) -> Format.sprintf
				"- (%s,%s)\n%s"
				"□" (print_term t2)
				(string_of_cont k)
			| Ktuple2_2 (v1, ty1, k) -> Format.sprintf
				"- (%s,%s)\n%s"
				(print_typed_value ty1 v1) "□"
				(string_of_cont k)
			| Ktuple3_1 (_, t2, t3, k) -> Format.sprintf
				"- (%s,%s,%s)\n%s"
				"□" (print_term t2) (print_term t3)
				(string_of_cont k)
			| Ktuple3_2 (v1, ty1, _, t3, k) -> Format.sprintf
				"- (%s,%s,%s)\n%s"
				(print_typed_value ty1 v1) "□" (print_term t3)
				(string_of_cont k)
			| Ktuple3_3 (v1, ty1, v2, ty2, k) -> Format.sprintf
				"- (%s,%s,%s)\n%s"
				(print_typed_value ty1 v1) (print_typed_value ty2 v2) "□"
				(string_of_cont k)
			| Kmatch (_, psl, k) -> sprintf
				"- match □ with\n%s\nend%s"
				(List.map (fun (p, s) ->
					sprintf "| %s → %s" (print_pattern p) (print_skeleton s)) psl
				|> String.concat "\n") (string_of_cont k)
			| Klet (_, p, sk, k) -> sprintf
				"- let %s = □ in\n%s\n%s"
				(print_pattern p) (print_skeleton sk) (string_of_cont k)
			| Kapply_1 (_, x, k) -> sprintf
				"- □ %s\n%s" (print_term x) (string_of_cont k)
			| Kapply_2 (f, ty, k) -> sprintf
				"- %s □\n%s" (print_typed_value ty f) (string_of_cont k)
			| Kapply_3 (ty, v, k) -> sprintf
				"- □ %s\n%s" (print_typed_value ty v) (string_of_cont k)
			| Kfield _ -> .
			| Knth2_1 k -> sprintf
				"- □.1\n%s" (string_of_cont k)
			| Knth2_2 k -> sprintf
				"- □.2\n%s" (string_of_cont k)
			| Knth3_1 k -> sprintf
				"- □.1\n%s" (string_of_cont k)
			| Knth3_2 k -> sprintf
				"- □.2\n%s" (string_of_cont k)
			| Knth3_3 k -> sprintf
				"- □.3\n%s" (string_of_cont k)
			| Krecmake _ -> .
			| Kset (_, ftl, k) -> sprintf
				"□ ← (%s)\n%s"
				(print_term_record ftl)
				(string_of_cont k)
			| Kset2 _ -> .
			end

		type 'a configuration =
			| Skel: env * 'a skeleton * ('a, 'b) cont * 'b fk -> 'b configuration
			| Term: env * 'a term * ('a, 'b) cont * 'b fk -> 'b configuration
			| Ret: 'a typed_value * 'a witness * ('a, 'b) cont * 'b fk -> 'b configuration
			| Fail: 'a fk -> 'a configuration
			| Apply: ('a -> 'b) typed_value * 'a typed_value * 'a witness * 'b witness * ('b, 'c) cont * 'c fk -> 'c configuration
			| CUnspec: 'a unspec_app * ('a, 'b) cont * 'b fk -> 'b configuration
			| CSpec: 'a spec_app * ('a, 'b) cont * 'b fk -> 'b configuration
		and 'a fk = 'a configuration Option.t

		let rec step: type a. a configuration -> a configuration =
			fun cfg ->
			begin match cfg with
			| Fail None -> (* computation failed *)
					Fail None
			| Fail (Some cfg) -> (* backtrack *)
					cfg
			| Term (e, t, k, fk) -> term_step e t k fk
			| Skel (e, sk, k, fk) -> skel_step e sk k fk
			| Ret (v, ty, k, fk) -> cont_step v ty k fk
			| Apply (f, x, tya, tyb, k, fk) -> apply_step f x tya tyb k fk
			| CUnspec (u, k, fk) -> unspec_step u k fk
			| CSpec (u, k, fk) -> spec_step u k fk
			end

		and term_step:
				type a b. env -> a term -> (a, b) cont -> b fk -> b configuration =
			fun e t k fk ->
			begin match t with
			| Tt -> Ret (Value (), Unit, k, fk)
			| Var (x, tya) ->
					begin match SMap.find_opt x e with
					| None -> failwith ("Unbound variable " ^ x)
					| Some (Pack (Term t, tyv)) ->
							Term (e, cast_term tyv tya t, k, fk)
					| Some (Pack (v, tyv)) -> Ret (cast_val tyv tya v, tya, k, fk)
					end
			| Spec s -> CSpec (PSSpec s, k, fk)
			| Unspec s -> CUnspec (PUUnspec s, k, fk)
			| Constr (c, t) -> Term (e, t, Kconstr (c, k), fk)
			| Func (p, s) ->
					Ret (Clos (e, p, s), Arrow (pattern_type p, skel_type s), k, fk)
			| Field _ -> .
			| Tuple2 (t0, t1) -> Term (e, t0, Ktuple2_1 (e, t1, k), fk)
			| Tuple3 (t0, t1, t2) -> Term (e, t0, Ktuple3_1 (e, t1, t2, k), fk)
			| Nth2_1 t -> Term (e, t, Knth2_1 k, fk)
			| Nth2_2 t -> Term (e, t, Knth2_2 k, fk)
			| Nth3_1 t -> Term (e, t, Knth3_1 k, fk)
			| Nth3_2 t -> Term (e, t, Knth3_2 k, fk)
			| Nth3_3 t -> Term (e, t, Knth3_3 k, fk)
			| RecMake (_, TNil) -> Fail fk
			| RecMake _ -> .
			| RecSet (t, set) -> Term (e, t, Kset (e, set, k), fk)
			end

		and skel_step:
				type a b. env -> a skeleton -> (a, b) cont -> b fk -> b configuration =
			fun e sk k fk ->
			begin match sk with
			| Return t -> Term (e, t, k, fk)
			| Branching (_, []) -> Fail fk
			| Branching (_, [s]) -> Skel (e, s, k, fk)
			| Branching (ty, s1::sq) ->
					(* if branch 1 fails, take other branches before going to fk *)
					let fk = Some (Skel (e, Branching (ty, sq), k, fk)) in
					Skel (e, s1, k, fk)
			| Match (t, psl) -> Skel (e, t, Kmatch (e, psl, k), fk)
			| LetIn (p, s1, s2) -> Skel (e, s1, Klet (e, p, s2, k), fk)
			| Apply (f, x) -> Term (e, f, Kapply_1 (e, x, k), fk)
			end

		and cont_step: type a b.
				a typed_value -> a witness -> (a, b) cont -> b fk -> b configuration =
			fun v ty k fk ->
			begin match k with
			| MarkStep k -> cont_step v ty k fk
			| Kid -> Ret (v, ty, Kid, fk) (* Computation over *)
			| Kconstr (c, k) ->
				Ret (Value (constr_compute c @@ from_typed_value v), constr_type c, k, fk)
			| Ktuple2_1 (e, t2, k) ->
				Term (e, t2, Ktuple2_2 (v, ty, k), fk)
			| Ktuple2_2 (v1, ty1, k) ->
				Ret (VTuple2 (v1, v), Tuple2 (ty1, ty), k, fk)
			| Ktuple3_1 (e, t2, t3, k) ->
				Term (e, t2, Ktuple3_2 (v, ty, e, t3, k), fk)
			| Ktuple3_2 (v1, ty1, e, t3, k) ->
				Term (e, t3, Ktuple3_3 (v1, ty1, v, ty, k), fk)
			| Ktuple3_3 (v1, ty1, v2, ty2, k) ->
				Ret (VTuple3 (v1, v2, v), Tuple3 (ty1, ty2, ty), k, fk)
			| Kmatch (_, [], _) -> Fail fk
			| Kmatch (e, (p, sk)::psl, k) ->
				begin match extend_env e p v with
				| None -> Ret (v, ty, Kmatch (e, psl, k), fk)
				| Some e' -> Skel (e', sk, k, fk)
				end
			| Klet (e, p, s, k) ->
				begin match extend_env e p v with
				| None -> Fail fk
				| Some e' -> Skel (e', s, k, fk)
				end
			| Kapply_1 (e, x, k) ->
				Term (e, x, Kapply_2 (v, ty, k), fk)
			| Kapply_2 (f, ty, k) ->
				begin match ty with
				| Arrow (tya, tyb) -> Apply (f, v, tya, tyb, k, fk)
				| _ -> failwith "Can only apply arrow types"
				end
			| Kapply_3 (_tyv, w, k) ->
				begin match ty with
				| Arrow (tya, tyb) -> Apply (v, w, tya, tyb, k, fk)
				| _ -> failwith "Can only apply arrow types"
				end
			| Kfield _ -> .
			| Knth2_1 k ->
				let (v, _) = untuple2 v in
				let (ty, _) = untuple2_type ty in
				Ret (v, ty, k, fk)
			| Knth2_2 k ->
				let (_, v) = untuple2 v in
				let (_, ty) = untuple2_type ty in
				Ret (v, ty, k, fk)
			| Knth3_1 k ->
				let (v, _, _) = untuple3 v in
				let (ty, _, _) = untuple3_type ty in
				Ret (v, ty, k, fk)
			| Knth3_2 k ->
				let (_, v, _) = untuple3 v in
				let (_, ty, _) = untuple3_type ty in
				Ret (v, ty, k, fk)
			| Knth3_3 k ->
				let (_, _, v) = untuple3 v in
				let (_, _, ty) = untuple3_type ty in
				Ret (v, ty, k, fk)
			| Krecmake _ -> .
			| Kset (_, TNil, k) -> Ret (v, ty, k, fk)
			| Kset _ -> .
			| Kset2 _ -> .
			end

		and apply_step:
				type a b c. (a -> b) typed_value -> a typed_value -> a witness
				-> b witness -> (b, c) cont -> c fk -> c configuration =
			fun f x tya tyb k fk ->
			begin match f with
			| Clos (e, p, s) ->
					begin match extend_env e p x with
					| None -> Fail fk
					| Some e' -> Skel (e', s, k, fk)
					end
			| Value f -> Ret (Value (f @@ from_typed_value x), tyb, k, fk)
			| Term _ -> failwith "Unexpected behaviour"
			| VUnspec u -> CUnspec (PUApp (u, tya, x), k, fk)
			| VSpec u -> CSpec (PSApp (u, tya, x), k, fk)
			end

		and unspec_step:
				type a b. a unspec_app -> (a, b) cont -> b fk -> b configuration =
			fun u k fk ->
			let ty = partial_unspec_type u in
			begin match ty with
			| Arrow _ -> step @@ Ret (VUnspec u, ty, k, fk)
			| _ -> Ret (Value (partial_unspec_compute u), ty, k, fk)
			end

		and spec_step:
				type a b. a spec_app -> (a, b) cont -> b fk -> b configuration =
			fun s k fk ->
			let ty = partial_spec_type s in
			begin match ty with
			| Arrow _ -> step @@ Ret (VSpec s, ty, k, fk)
			| _ -> partial_spec_compute s k fk
			end

		and partial_spec_compute:
				type a b. a spec_app -> (a, b) cont -> b fk -> b configuration =
			fun s k fk ->
			begin match s with
			| PSSpec s -> Term (SMap.empty, spec_compute s, k, fk)
			| PSApp (f, tyx, x) ->
					let newk = (* continuation for f : evaluate then apply to x *)
						Kapply_3 (tyx, x, k) in
					partial_spec_compute f newk fk
			end
	end

	open AbstractMachine

	type action =
		| Next | Previous
		| FastForward | Start
		| Step
		| GoTo of Int.t
		| Breakpoint of String.t

	type 'a amstate = {
		cpt: Int.t ;
		beg: 'a configuration ;
		cur: 'a configuration ;
		prev: 'a configuration List.t ;
	}

	let over: type a. a configuration -> Bool.t =
		begin function
		| Fail None -> true
		| Ret (_, _, Kid, _) -> true
		| _ -> false
		end

	let (let*) = Result.bind

	let rec perform action log =
		begin match action with
		| Next ->
				let cpt = log.cpt + 1 in
				let prev = log.cur :: log.prev in
				let cur = step log.cur in
				Ok {log with cpt;cur;prev}
		| Previous ->
				begin match log.prev with
				| [] ->
						Error "There is no previous step."
				| a :: q ->
						let cur = a in
						let prev = q in
						let cpt = log.cpt - 1 in
						Ok {log with cur;prev;cpt}
				end
		| GoTo i ->
				let rec iter x n log =
					if n = 0 then
						Ok log
					else
						let* log = perform x log in
						iter x (n - 1) log
				in
				if i < 0 then
					Error "Cannot go to negative step"
				else if i < log.cpt then
					iter Previous (log.cpt - i) (log)
				else if i > log.cpt then
					iter Next (i - log.cpt) (log)
				else
					Ok log
		| Breakpoint term_name ->
				let rec next log =
					let* log = perform Next log in
					loop log
				and loop log =
					let continue =
						begin match log.cur with
						| CUnspec (u, _, _) ->
								begin match partial_unspec_type u with
								| Arrow _ -> true
								| _ -> term_name <> "*" && partial_unspec_name u <> term_name
								end
						| CSpec (s, _, _) ->
								begin match partial_spec_type s with
								| Arrow _ -> true
								| _ -> term_name <> "*" && partial_spec_name s <> term_name
								end
						| _ -> not (over log.cur)
						end
					in
					if continue then next log else Ok log
				in
				loop log
		| Start ->
				let cpt = 0 in
				let beg = log.beg in
				let cur = beg in
				let prev = [] in
				Ok {log with cpt; cur; prev}
		| FastForward ->
				if over log.cur then Ok log else
				let* log = perform Next log in
				perform FastForward log
		| Step ->
				let rec compute_step log =
					begin match log.cur with
					| Fail _ -> Ok log
					| Ret (x, ty, MarkStep k, f) ->
							Ok {log with cur = Ret (x, ty, k, f)}
					| _ ->
							let* log = perform Next log in
							compute_step log
					end
				in
				begin match log.cur with
				| Fail _ -> Ok log
				| Ret _ -> Ok log
				| Skel (e, sk, opl, f) ->
						compute_step ({log with cur = Skel (e, sk, MarkStep opl, f)})
				| Term (e, t, opl, f) ->
						compute_step ({log with cur = Term (e, t, MarkStep opl, f)})
				| Apply (f, x, tya, ty, k, fk) ->
						compute_step ({log with cur = Apply (f, x, tya, ty, MarkStep k, fk)})
				| CUnspec (u, k, fk) ->
						compute_step ({log with cur = CUnspec (u, MarkStep k, fk)})
				| CSpec (s, k, fk) ->
						compute_step ({log with cur = CSpec (s, MarkStep k, fk)})
				end
		end

	let compute: type a. a amstate -> a Option.t =
		fun log ->
			begin match perform FastForward log with
			| Error _ -> assert false
			| Ok log ->
					begin match log.cur with
					| Fail None -> None
					| Ret (v, _, Kid, _) -> Some (from_typed_value v)
					| _ -> assert false (* FastForward failed *)
					end
			end

	let rec string_of_bt (bt:'a configuration option): String.t =
		begin match bt with
		| None -> ""
		| Some (Skel (_, s, _, bt')) ->
				"- " ^ print_skeleton s ^ "\n" ^ string_of_bt bt'
		| _ -> failwith "Internal error: backtracking point is not a branching"
		end

		type state_kind = | Term | Skeleton | Value | Fail | Apply

		type printing =
			{ environment: String.t
			; continuation: String.t
			; current: String.t
			; backtrack: String.t
			; counter: Int.t
			; kind: state_kind
			}

	let mkprint environment continuation current backtrack counter kind =
			{ environment ; continuation ; current ; backtrack ; counter ; kind}

	let print_state: type a. a amstate -> printing =
		fun a ->
		begin match a.cur with
		| Fail None ->
				mkprint
				""
				""
				""
				"No result"
				a.cpt
				Fail
		| Fail (Some bt) ->
				mkprint
				""
				""
				"Failure in execution, backtracking"
				(string_of_bt (Some bt))
				a.cpt
				Fail
		| Ret (v, ty, Kid, bt) ->
				mkprint
				""
				""
				(print_typed_value ty v)
				(string_of_bt bt)
				a.cpt
				Value
		| Ret (v, ty, k, bt)->
				mkprint
				(print_env SMap.empty)
				(string_of_cont k)
				(print_typed_value ty v)
				(string_of_bt bt)
				a.cpt
				Value
		| Term (e, t, k, bt) ->
				mkprint (print_env e)
				(string_of_cont k)
				(print_term t)
				(string_of_bt bt)
				a.cpt
				Term
		| Skel (e, sk, k, bt) ->
				mkprint (print_env e)
				(string_of_cont k)
				(print_skeleton sk)
				(string_of_bt bt)
				a.cpt
				Skeleton
		| Apply (f, x, tya, tyb, k, bt) ->
				mkprint
				""
				(string_of_cont k)
				(
					print_typed_value (Arrow (tya, tyb)) f  ^
					" " ^
					print_typed_value tya x)
				(string_of_bt bt)
				a.cpt
				Apply
		| CUnspec (u, k, bt)->
				mkprint
				(print_env SMap.empty)
				(string_of_cont k)
				(print_typed_value (partial_unspec_type u) (VUnspec u))
				(string_of_bt bt)
				a.cpt
				Apply
		| CSpec (s, k, bt)->
				mkprint (print_env SMap.empty)
				(string_of_cont k)
				(print_typed_value (partial_spec_type s) (VSpec s))
				(string_of_bt bt)
				a.cpt
				Apply
		end

	let apply_eval_expr a1 =
		let state =
			CSpec (
				PSApp (PSSpec S_eval_expr,
					Tuple2 (State, Expr), Value a1),
				Kid, None)
		in {beg=state; cur=state; prev=[]; cpt=0}
	let apply_eval_stmt a1 =
		let state =
			CSpec (
				PSApp (PSSpec S_eval_stmt,
					Tuple2 (State, Stmt), Value a1),
				Kid, None)
		in {beg=state; cur=state; prev=[]; cpt=0}
	let apply_neg a1 =
		let state =
			CSpec (
				PSApp (PSSpec S_neg,
					Boolean, Value a1),
				Kid, None)
		in {beg=state; cur=state; prev=[]; cpt=0}
end
