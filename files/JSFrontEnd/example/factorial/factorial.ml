open While_backend

module SMap = Map.Make(String)

module rec T: sig
	type ident = string
	type lit = int
	type state = Spec(T).value SMap.t
	type vint = int
end = T

module S = struct
	include Spec(T)

	let add (x, y) = x + y
	let eq (x, y) = if x = y then True else False
	let litToVal x = Int x
	let read (x, s) = SMap.find x s
	let write (x, s, v) = SMap.add x v s

	let print_ident = Fun.id
	let print_lit = string_of_int
	let print_value v =
		begin match v with
		| Bool True -> "⊤"
		| Bool False -> "⊥"
		| Int i -> string_of_int i
		end
	let print_state x =
		SMap.bindings x |> List.map (fun (x, v) ->
			x ^ " ← " ^ print_value v) |> String.concat "; " |>
			Format.sprintf "{%s}"
end
open S

module P = struct
	include Print(S)
	let print_value = S.print_value
end


let fact (n: int) =
	Seq (Assign ("n", Const n),                           (* n := n          *)
	Seq (Assign ("fact", Const 1),                        (* fact := 1       *)
	While( Not (Equal (Var "n", Const 0)),                (* while !(n = 0)  *)
	  Seq (Assign ("a", Var "fact"),                      (*   a := fact     *)
	  Seq (Assign ("i", Const 1),                         (*   i := 1        *)
	  Seq (While (Not (Equal (Var "i", Var "n")),         (*   while ! (i=n) *)
	    Seq (Assign ("fact", Plus (Var "fact", Var "a")), (*     fact += a   *)
	    Assign ("i", Plus (Var "i", Const 1)))),          (*     i += 1      *)
	  Assign ("n", Plus (Var "n", Const (-1)))))))))      (*   n += -1       *)




module Debugger = While_backend.MakeDebugger(P)
module Interface = Js_of_ocaml_frontend.MakeJSInterface(Debugger)

let () =
	let state = Debugger.apply_eval_stmt (SMap.empty ,fact 4) in
	Interface.debug state
