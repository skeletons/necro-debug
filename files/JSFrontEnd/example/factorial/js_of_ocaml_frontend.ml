open Js_of_ocaml
open Dom_html

module type DEBUGGER = sig
		type action =
			| Next | Previous
			| FastForward | Start
			| Step
			| GoTo of Int.t
			| Breakpoint of String.t

		type _ amstate

		val perform: action -> 'a amstate -> ('a amstate, String.t) Result.t

		(* printing *)
		type state_kind = | Term | Skeleton | Value | Fail | Apply
		type printing =
			{ environment: String.t
			; continuation: String.t
			; current: String.t
			; backtrack: String.t
			; counter: Int.t
			; kind: state_kind
			}
		val print_state: _ amstate -> printing
end


module MakeJSInterface(D:DEBUGGER): sig
	val debug: _ D.amstate -> unit
end = struct
	let errorlog str =
		let err_div = getElementById "fail" in
		err_div##.innerHTML := Js.string str
	let empty_errorlog () = errorlog ""

	let help_message = String.concat "\n\t"
		["Help:";
		"←←: Go to step 0";
		"←: Go to previous step";
		"→: Go to next step";
		"→→: Go to result";
		"step: Compute current expression to a value";
		"go to: Go to an arbitrary step"]

		type log =
			Log: _ D.amstate -> log

		let log: log option ref = ref None


	(* print the new counter *)
	let update_cpt cpt =
		let cpt_span = getElementById "cpt" in
		cpt_span##.innerHTML := (Js.string (string_of_int cpt))

	let print_cur_env env =
		let env_div = getElementById "env" in
		env_div##.innerHTML := Js.string env

	let print_cont cont =
		let cont_div = getElementById "cont" in
		cont_div##.innerHTML := Js.string cont

	let print_cur cur =
		let comp_div = getElementById "comp" in
		comp_div##.innerHTML := Js.string cur

	let print_bt bt =
		let bt_div = getElementById "backtrack" in
		bt_div##.innerHTML := Js.string bt

	let print_state: type a. a D.amstate -> D.printing =
		fun st ->
			let fix_html str =
				let replace str1 str2 str =
					Str.global_replace (Str.regexp_string str1) str2 str
				in
				str
				|> replace "<" "&lt;"
				|> replace ">" "&gt;"
				|> replace "\n" "<br>"
				|> replace " " "&nbsp;"
			in
			let print = D.print_state st in
			{ environment = fix_html print.environment
			; continuation = fix_html print.continuation 
			; current = fix_html print.current
			; backtrack = fix_html print.backtrack
			; counter = print.counter
			; kind = print.kind }

	let print_amstate: type a. a D.amstate -> Unit.t =
		fun a -> empty_errorlog ();
		let p = print_state a in
		update_cpt p.counter ;
		begin match p.kind with
		| Fail when p.backtrack = "" ->
				print_cur_env "" ;
				print_cont "" ;
				print_cur "Execution over, no backtracking point available, \
				no result reached."
		| Fail ->
				print_cur_env "" ;
				print_cont "" ;
				print_cur "Failure in execution, backtracking" ;
				print_bt (p.backtrack)
		| Value when p.continuation = "" ->
				print_cur_env "" ;
				print_cont "" ;
				print_cur ("Execution over. Returned value: " ^ p.current) ;
				print_bt p.backtrack
		| Value ->
				print_cur_env "";
				print_cont p.continuation ;
				print_cur ("<span class=\"value\">" ^ p.current ^ "</span>") ;
				print_bt p.backtrack
		| Term ->
				print_cur_env p.environment;
				print_cont p.continuation ;
				print_cur ("<span class=\"term\">" ^ p.current ^ "</span>") ;
				print_bt p.backtrack
		| Skeleton ->
				print_cur_env p.environment;
				print_cont p.continuation;
				print_cur ("<span class=\"skel\">" ^ p.current ^ "</span>") ;
				print_bt p.backtrack
		| Apply ->
				print_cur_env "" ;
				print_cont p.continuation;
				print_cur (
					"<span class=\"skel\">" ^ p.current ^ "</span>") ;
				print_bt p.backtrack
		end

	(* Print the current log *)
	let print_log (Log log) =
		print_amstate log

		let perform action () =
			begin match !log with
			| None -> errorlog "Internal error"
			| Some (Log x) ->
					begin match D.perform action x with
					| Ok l ->
							print_log (Log l);
							log := Some (Log l)
					| Error str -> errorlog str
					end
			end

		let init_buttons =
			let make id action =
				let button = getElementById id in
				button##.onclick := handler (fun _ ->
					action (); Js._false)
			in
			let goto () =
				let i = window##prompt (Js.string "What step") (Js.string "") in
				begin match Js.Opt.to_option i with
				| None -> errorlog "Empty prompt"
				| Some i ->
						begin match int_of_string_opt (Js.to_string i) with
						| None -> errorlog (Js.to_string i ^ " is not a number")
						| Some i -> perform (GoTo i) ()
						end
				end
			in
			let help () =
				window##alert (Js.string help_message)
			in
			let bpt log =
				let bpt = getElementById_coerce "breakpointterm" CoerceTo.select in
				let term_name =
					begin match bpt with
					| None -> failwith "Internal error"
					| Some x -> (Js.to_string x##.value)
					end
				in
				perform (Breakpoint term_name) log
			in
			make "prev"         (perform Previous) ;
			make "next"         (perform Next) ;
			make "step"         (perform Step) ;
			make "ff"           (perform FastForward) ;
			make "fst"          (perform Start) ;
			make "goto"         goto ;
			make "help"         help ;
			make "breakpoint"   bpt ;
			let bpt = getElementById "breakpointterm" in
			bpt##.innerHTML :=
				Js.string (Js.to_string (bpt##.innerHTML)
				^ "<option value=\"add\">add</option>"
				^ "<option value=\"apply\">apply</option>"
				^ "<option value=\"div\">div</option>"
				^ "<option value=\"eval\">eval</option>"
				^ "<option value=\"litToVal\">litToVal</option>"
				^ "<option value=\"mult\">mult</option>"
				^ "<option value=\"sub\">sub</option>")

		let debug: type a. a D.amstate -> Unit.t =
			fun state -> errorlog "Bonjour";
			window##.onload := handler (fun _ ->
				init_buttons;
				log := Some (Log state);
				print_log (Log state);
				Js._false)

end
