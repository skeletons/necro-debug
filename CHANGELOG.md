# Changelog

# 0.7.6
- Update to Necro Lib v0.16.0, without handling modules

# 0.7.5
- Fix printing of arrows

# 0.7.4
- Update to Necro Lib v0.15.1

# 0.7.3
- Protect against ocaml keywords

# 0.7.2
- Update to Necro Lib v0.14.7

# 0.7.1
- Add two actions to backend (and consequently front-end) :
  drop current branch, and pick branch number `n`
- Fix drop-down for breakpoints

# 0.7
- Separate front-end from back-end. The back-end is generated, and the
  front-end is generic. Only JS of OCaml for now

# 0.6.8
- Update to Necro Lib 0.14
- Handle terms of the form `t.3`

# 0.6.7.1
- If no such application, button "next application of" goes to end result
- Use an option list to choose term name, instead of a text field
- Fix issue #6

# 0.6.7
- Add a button to stop at the application of a term

# 0.6.6.2
- Fix: remove aliases from witnesses
- Fix: add typing information in case Skel file uses `Some` or `None` as
constructor

# 0.6.6.1
- Fix

# 0.6.6
- Print backtracking points, and implement the "?" button

# 0.6.5
- When a constructor has an argument of type unit, use no argument in the
  generated OCaml

# 0.6.4.1
- Add a comment on the first line of the generated file

# 0.6.4
- Fix some of the "unused variable" warnings in generated files. Now, no warning
  on a file with no field.

# 0.6.3.1
- Fix a problem that happened when working on files with no constructors

# 0.6.3
- Better printing of skeletons

# 0.6.2.5
- Fix

# 0.6.2.4
- Fix

# 0.6.2.3
- Fix

# 0.6.2.2
- Fix

# 0.6.2.1
- Fix

# 0.6.2
- Delay application of specified and unspecified terms

# 0.6.1
- Fix with builtin types, replacing them (e.g `string` by `String.t`)

# 0.6
- New version of Necro Debug from the bottom up. This version has a shallow
  embedding for types, and deep embedding for terms. It does not support
  aliases yet, but you can use necro trans to remove them beforehand

# 0.5.4
- Fix issue #3

# 0.5.3
- Fix issue #2

# 0.5.2
- Fix issue #1

# 0.5.1
- Fix on records

# 0.5
- Add records

# 0.4
- Add a step function to debugger, in order to compute an expression faster

# 0.3.1
- Update to Necro Lib 0.12

# 0.3
- Update to Necro Lib 0.11

# 0.2.4
- Switch to opam

# 0.2.3.1
- Fix makefile so that opam works

# 0.2.3
- Update to Necro Lib 0.10 (without switching to dune)

# 0.2.2
- Removing `jsofocaml` from template's name
- Renaming `Debugger` functor to `MakeDebugger`

# 0.2.1
- Fix printing

# 0.2
- Update to Necro Lib version 0.9

# 0.1.1
- Add a step to application with all arguments shown before evaluating the application

# Version 0.1
- Initial version
