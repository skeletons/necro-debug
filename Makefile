SHELL=bash

NAME := necro-debug
# WARNING : GIT TAG is prefixed with "v"
VERSION := 0.7.6
TGZ := $(NAME)-v$(VERSION).tar.gz
MD5TXT := $(TGZ).md5sum.txt
OPAM_NAME := necrodebug

# GIT VARIABLES
GITLAB := gitlab.inria.fr
GITPROJECT := skeletons
GIT_REPO := git@$(GITLAB):$(GITPROJECT)/$(NAME).git
GIT_RELEASE := https://$(GITLAB)/$(GITPROJECT)/$(NAME)/-/archive/v$(VERSION)/$(TGZ)

# OPAM VARIABLES
OPAM_REPO_GIT := ../opam-repository
OPAM_FILE := packages/$(OPAM_NAME)/$(OPAM_NAME).$(VERSION)/opam

##########################################################################################
#                       TARGET                                                           #
##########################################################################################

nothing:

# Change version number everywhere where it's specified, and update Makefile
new_version:
	@rm -f $(TGZ) $(MD5TXT)
	@read -p "Please enter new version (current is $(VERSION)): " g;\
	perl -i -pe "s#archive/v$(VERSION)#archive/v$$g#" ./necrodebug.opam.template;\
	perl -i -pe "s#v$(VERSION).tar.gz#v$$g.tar.gz#" ./necrodebug.opam.template;\
	perl -i -pe "s/^VERSION := $(VERSION)/VERSION := $$g/" ./Makefile;\
	perl -i -pe "s/^\(version $(VERSION)\)$$/\(version $$g\)/" ./dune-project;\
	git commit ./dune-project ./Makefile ./necrodebug.opam.template -m "Release version $$g";\
	git tag -a v$$g -fm 'OPAM necrodebug package for deployment'
	git push --follow-tags

opam_update :
	rm -f $(TGZ) $(MD5TXT)
	wget -q $(GIT_RELEASE)
	md5sum $(TGZ) > $(MD5TXT)
	perl -i -pe 's|^\s*checksum: "md5\s*=.*"| checksum: "md5='$$(cat $(MD5TXT) | cut -d" " -f 1)'"|' necrodebug.opam.template
	dune build
	mkdir -p $(OPAM_REPO_GIT)/packages/$(OPAM_NAME)/$(OPAM_NAME).$(VERSION)/
	cp necrodebug.opam $(OPAM_REPO_GIT)/$(OPAM_FILE)

release:
	cd ../opam-repository; \
		git pull 2>/dev/null || true
	make new_version
	make opam_update
	cd ../opam-repository; \
		git add packages/necrodebug && git commit -am "Push new Necro Debug version" && git push

rerelease:
	@git tag -f v$(VERSION)
	@git push --tags --force
	make opam_update
	cd ../opam-repository; \
		git add packages/necrodebug && git commit -am "Upstream change for Necro Debug" && git push


.PHONY: opam_update new_version release rerelease nothing
